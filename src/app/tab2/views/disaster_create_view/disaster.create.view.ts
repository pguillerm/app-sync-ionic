import {Component, OnInit}               from '@angular/core';
import {ActivatedRoute,Router}           from '@angular/router';
import {Camera}                          from '@ionic-native/camera/ngx';
import {FormGroup,FormControl}           from '@angular/forms';
import {DaoFactory,Dao}                  from './../../../modules/commons/services/dao/dao.factory';
import {Disaster,DisasterStatus,Rent}    from './../../../modules/commons/models/commons.models.interfaces';
import {EventsServices,AppEvents}        from './../../../modules/commons/services/events.services';
    

@Component({
    templateUrl   : './disaster.create.view.html',
    styleUrls     : ['./disaster.create.view.scss'],
    providers     : [Camera]
})
export class DisasterCreateView implements OnInit{
    /***************************************************************************
    * ATTRIBUTE
    ***************************************************************************/
    gobackUrl               : string = "/tab2";
    step                    : number = 0;
    rents                   : Rent[];
    disasterTypes           : string[] = ["Plomberie","Electricité","Chauffage","autre"];
    disasterForm            : FormGroup;

    showOtherDate           : boolean = false;
    showOtherDisasterType   : boolean = false;
    cameraOptions           : any;
    disableCamera           : boolean = false;
    currentPhoto            : string;

    selectedRent            : Rent;

    private dao             : Dao;
    
    /***************************************************************************
    * CONSTRUCTORS
    ***************************************************************************/
    constructor(private camera          : Camera,
                private router          : Router,
                private daoFactory      : DaoFactory,
                private eventsService   : EventsServices) {
        this.dao = this.daoFactory.getDao();
    }
    
    
    ngOnInit(){
        this.initForm();
        this.cameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        };

        this.dao.findAllRents().then(data=>this.rents=data);
    }
    
    initForm(){
        this.disasterForm = new FormGroup({
            rentUid: new FormControl(),
            disasterType : new FormControl(),
            date : new FormControl(),
            image : new FormControl()
        });
    }
    /***************************************************************************
    * FORM FUNCTIONS
    ***************************************************************************/
    next(){
        this.step++;  
    }
    defineRent(rent:Rent){
        this.selectedRent = rent;
        this.disasterForm.get("rentUid").setValue(rent.id);
        this.next();
    }


    defineDisasterType(value:string){
        if(value=="autre"){
            this.showOtherDisasterType = true;
        }else{
            this.disasterForm.get("disasterType").setValue(value);
            this.next();
        }
    }


    defineIsNow(){
        this.showOtherDate=false;
        let now = new Date();
        let date = [now.getFullYear(),now.getMonth()+1,now.getDate()];

        let result = date.join('-');
        console.log("date : ",result);
        this.disasterForm.get("date").setValue(result);
        this.next();
    }

    editOtherDate(){
        this.showOtherDate=true;
    }

    takePhoto(){
        this.enableSubApp(true);
        this.camera.getPicture(this.cameraOptions).then((imageData) => {
            this.enableSubApp(false);
            console.log("imageData : "+imageData);
            if(imageData!=null){
                let base64Image = 'data:image/jpeg;base64,' + imageData;
                this.disasterForm.get("image").setValue(base64Image);
                this.currentPhoto = base64Image;
                setTimeout(()=>this.next(),1000);
            }

        }, (err) => {
            console.error("error on loading image :"+JSON.stringify(err));
            this.disableCamera=true;
            this.enableSubApp(false);
        });
    }


    send(){
        let disaster = this.buildDisasterObject();
        this.dao.saveDisaster(disaster)
                .then(()=>{
                    this.dao.findAllDisasters().then(allDisasters=>{
                            setTimeout(()=>{
                                console.log("re init");
                                this.initForm();
                                this.router.navigate([this.gobackUrl]);
                            },1000);
                        });
                    })
                    
        
    }
    /***************************************************************************
    * TOOLS
    ***************************************************************************/
    buildDisasterObject() : Disaster{
        let disasterDate = new Date();
        let rawDate      = this.disasterForm.get("date").value;

        if(rawDate != null){
            let datePart = rawDate.split("-").map(item=>Number(item));
            disasterDate.setFullYear(datePart[0]);
            disasterDate.setMonth(datePart[1]-1);
            disasterDate.setDate(datePart[2]);
        }
        
        disasterDate.setHours(12);
        disasterDate.setMinutes(0);
        disasterDate.setSeconds(0);

        return {
            type : {id:this.disasterForm.get("disasterType").value},
            date : disasterDate.getTime(),
            status : DisasterStatus.unread,
            rendUid : this.disasterForm.get("rentUid").value
        };
    }
    
    resolveQuestionClass(index:number):string{
        let result = ['question'];
        
        if(index == this.step){
            result.push("current");
        }else if(index < this.step){
            result.push("previous");
        }else{
            result.push("next");
        }
    
        return result.join(' ');
    }

    enableSubApp(status : boolean){
        if(status){
            this.eventsService.fireEvent(AppEvents.SUB_APPLICATION_START);
        }else{
            this.eventsService.fireEvent(AppEvents.SUB_APPLICATION_DONE);
        }
    }


    get disasterDate():string{
        return this.disasterForm.get("date").value;
    }

    get imagePhoto():string{
        return this.disasterForm.get("image").value;
    }


    
}