import {Component, OnInit}               from '@angular/core';
import {ActivatedRoute,Router}           from '@angular/router';
import {IconDefinition}                  from '@fortawesome/fontawesome-common-types';
import {Icons}                           from './../../../modules/commons/models/icons';

import {DaoFactory,Dao}                  from './../../../modules/commons/services/dao/dao.factory';
import {Disaster,Rent}                   from './../../../modules/commons/models/commons.models.interfaces';
import {isNotNull}                       from 'src/app/modules/commons/tools/checks';


@Component({
    templateUrl   : './disaster.detail.view.html',
    styleUrls     : ['./disaster.detail.view.scss'],
    providers     : []
})
export class DisasterDetailView implements OnInit{

    /***************************************************************************
    * ATTRIBUTE
    ***************************************************************************/
    private dao             : Dao;
    private disaster        : Disaster;
    private rent            : Rent;

    private goBackIcon      : IconDefinition = Icons.goBack;
    private saveIcon        : IconDefinition = Icons.validate;
    /***************************************************************************
    * CONSTRUCTORS
    ***************************************************************************/
    constructor(private router          : Router,
                private route           : ActivatedRoute,
                private daoFactory      : DaoFactory) {
        this.dao = this.daoFactory.getDao();
    }
    
    ngOnInit() {
        this.route.paramMap.subscribe((params)=>{
            let uid = params.get("uid");
            this.loadData(uid);
        });
    }
    

    loadData(uid:string){
        this.dao.getDisaster(uid).then(data=>{
            if(isNotNull(data)){
                console.log("DisasterDetailView disaster : ",data);
                this.disaster=data;
    
                if(isNotNull(data.rendUid)){
                    this.dao.findRent(data.rendUid)
                            .then(rent=>{
                                console.log("DisasterDetailView rent : ",rent);
                                this.rent=rent;
                            });
                }
            }
        });
    }
    /***************************************************************************
    * ACTIONS
    ***************************************************************************/
   descriptionChange(event:any){
       this.disaster.description=event.target.value;
   }
    goBack(){
        this.router.navigate(["/tab2"]);
    }
    
    save(){
       this.dao.updateDisaster(this.disaster)
               .then(()=> console.log("udpdate disaster success"))
               .catch(error=>console.error(error)); 
    }


    /***************************************************************************
    * ACTIONS
    ***************************************************************************/
    get thumbnail() : string{
        let result = null;
        if(this.rent!=null && 
            this.rent.thumbnail !=null){
            result = this.rent.thumbnail.source;
        }
        return result;
    }
}