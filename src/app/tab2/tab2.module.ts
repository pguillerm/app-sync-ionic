import {IonicModule}                            from '@ionic/angular';
import {RouterModule}                           from '@angular/router';
import {NgModule}                               from '@angular/core';
import {CommonModule}                           from '@angular/common';
import {FormsModule,ReactiveFormsModule}        from '@angular/forms';
import {FontAwesomeModule}                      from '@fortawesome/angular-fontawesome';
import {Tab2Page}                               from './tab2.page';
import {DisasterCreateView}                     from './views/disaster_create_view/disaster.create.view';
import {DisasterDetailView}                     from './views/disaster_detail_view/disaster.detail.view'
import {CommonsModule}                          from './../modules/commons/commons.module';


@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    CommonsModule
  ],
  declarations: [
    Tab2Page,
    DisasterCreateView,
    DisasterDetailView
  ],
  exports:[
    Tab2Page,
    DisasterCreateView
  ]
})
export class Tab2PageModule {
  
}
