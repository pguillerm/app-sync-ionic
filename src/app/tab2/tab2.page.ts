import {Component}              from '@angular/core';
import {formatDate}             from '@angular/common';
import {Router,ActivatedRoute}  from '@angular/router';
import {IconDefinition}         from '@fortawesome/fontawesome-common-types';
import {Icons,getIcon}          from './../modules/commons/models/icons';
import {normalize}              from './../modules/commons/services/tools';
import {DaoFactory,Dao}         from './../modules/commons/services/dao/dao.factory';
import {Disaster}               from './../modules/commons/models/commons.models.interfaces';
import { isNull } from 'util';
@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  /***************************************************************************
  * ATTRIBUTE
  ***************************************************************************/
  private dao   : Dao;
  disasters     : Disaster[];
  deleteIcon    : IconDefinition = Icons.delete;
  /***************************************************************************
  * CONSTRUCTORS
  ***************************************************************************/
  constructor(private router: Router,
              private route: ActivatedRoute,
              private daoFactory : DaoFactory) {
    this.dao=this.daoFactory.getDao();

    this.route.params.subscribe(params => {
        console.log("this.route.params.subscribe");
        this.initializeDisasters();
    });
  }

  initializeDisasters(){
    this.dao.findAllDisasters().then(data =>{
      console.log("all disasters : ", data);
      this.disasters = data;
    });
  }
  /***************************************************************************
  * ACTIONS
  ***************************************************************************/
  createNewDisaster(){
    this.router.navigate(["/tab2/create"]);
  }
  
  deleteDisaster(disaster:Disaster){
    this.dao.deleteDisaster(disaster.id)
            .then(data=> this.initializeDisasters());
  }

  goToDetail(disaster:Disaster){
    this.router.navigate([`/tab2/view/${disaster.id}`]);
  }
  
  /***************************************************************************
  * TOOLS
  ***************************************************************************/
  resolveIcon(disaster:Disaster) : IconDefinition{
    return getIcon("defaultDisasterStatus");
  }


}
