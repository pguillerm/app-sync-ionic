import {NgModule}                         from '@angular/core';
import {BrowserModule}                    from '@angular/platform-browser';
import {RouteReuseStrategy}               from '@angular/router';
import {IonicModule, IonicRouteStrategy}  from '@ionic/angular';
import {SplashScreen}                     from '@ionic-native/splash-screen/ngx';
import {StatusBar}                        from '@ionic-native/status-bar/ngx';

import {AmplifyAngularModule,
        AmplifyService}                   from  'aws-amplify-angular';

import {FontAwesomeModule}                from '@fortawesome/angular-fontawesome';

import {AppRoutingModule}                 from './app-routing.module';
import {AppComponent}                     from './app.component';
import {CommonsModule}                    from './modules/commons/commons.module';
import {Network}                          from '@ionic-native/network/ngx';

import {AppSyncDao}                       from './modules/commons/services/dao/app.sync.dao';
import {DaoFactory}                       from './modules/commons/services/dao/dao.factory';
import {EventsServices}                   from './modules/commons/services/events.services';
import {AppSyncEntitiesMapperService}     from './modules/commons/services/dao/app.sync.entities.mapper.service';
import {AppSyncService}                   from './modules/commons/services/dao/app.sync.service';
import {CacheService}                     from './modules/commons/services/cache/cache.service';

import {Tab1PageModule}                   from './tab1/tab1.module';
import {Tab2PageModule}                   from './tab2/tab2.module';
import {Tab3PageModule}                   from './tab3/tab3.module';

import Amplify from 'aws-amplify';
import amplify from './../aws-exports';
Amplify.configure(amplify);

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    FontAwesomeModule,
    AppRoutingModule,
    CommonsModule,
    AmplifyAngularModule,
    Tab1PageModule,
    Tab2PageModule,
    Tab3PageModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Network,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    AppSyncDao ,
    DaoFactory,
    EventsServices,
    AmplifyService,
    AppSyncEntitiesMapperService,
    AppSyncService,
    CacheService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
