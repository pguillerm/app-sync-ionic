/* tslint:disable */
//  This file was automatically generated and should not be edited.
import { Injectable } from "@angular/core";
import API, { graphqlOperation } from "@aws-amplify/api";
import { GraphQLResult } from "@aws-amplify/api/lib/types";
import * as Observable from "zen-observable";

export type CreateARentInput = {
  id?: string | null;
  name: string;
  type?: ARentType | null;
  price: number;
  description?: string | null;
};

export enum ARentType {
  FLAT = "FLAT",
  HOME = "HOME",
  OFFICE = "OFFICE",
  PARKING = "PARKING"
}

export type UpdateARentInput = {
  id: string;
  name?: string | null;
  type?: ARentType | null;
  price?: number | null;
  description?: string | null;
};

export type DeleteARentInput = {
  id?: string | null;
};

export type CreateAImageInput = {
  id?: string | null;
  source: string;
};

export type UpdateAImageInput = {
  id: string;
  source?: string | null;
};

export type DeleteAImageInput = {
  id?: string | null;
};

export type CreateADisasterInput = {
  id?: string | null;
  date: number;
  aDisasterRentId?: string | null;
  aDisasterStatusId?: string | null;
};

export enum ACommentStatus {
  unread = "unread",
  read = "read"
}

export type UpdateADisasterInput = {
  id: string;
  date?: number | null;
  aDisasterRentId?: string | null;
  aDisasterStatusId?: string | null;
};

export type DeleteADisasterInput = {
  id?: string | null;
};

export type CreateADisasterStatusInput = {
  id?: string | null;
  value: string;
  aDisasterStatusDisasterId?: string | null;
};

export type UpdateADisasterStatusInput = {
  id: string;
  value?: string | null;
  aDisasterStatusDisasterId?: string | null;
};

export type DeleteADisasterStatusInput = {
  id?: string | null;
};

export type CreateADisasterTypeInput = {
  id?: string | null;
  value: string;
};

export type UpdateADisasterTypeInput = {
  id: string;
  value?: string | null;
};

export type DeleteADisasterTypeInput = {
  id?: string | null;
};

export type CreateACommentInput = {
  id?: string | null;
  login?: string | null;
  content?: string | null;
  status?: ACommentStatus | null;
  aCommentDisasterId?: string | null;
};

export type UpdateACommentInput = {
  id: string;
  login?: string | null;
  content?: string | null;
  status?: ACommentStatus | null;
  aCommentDisasterId?: string | null;
};

export type DeleteACommentInput = {
  id?: string | null;
};

export type ModelARentFilterInput = {
  id?: ModelIDFilterInput | null;
  name?: ModelStringFilterInput | null;
  type?: ModelARentTypeFilterInput | null;
  price?: ModelFloatFilterInput | null;
  description?: ModelStringFilterInput | null;
  and?: Array<ModelARentFilterInput | null> | null;
  or?: Array<ModelARentFilterInput | null> | null;
  not?: ModelARentFilterInput | null;
};

export type ModelIDFilterInput = {
  ne?: string | null;
  eq?: string | null;
  le?: string | null;
  lt?: string | null;
  ge?: string | null;
  gt?: string | null;
  contains?: string | null;
  notContains?: string | null;
  between?: Array<string | null> | null;
  beginsWith?: string | null;
};

export type ModelStringFilterInput = {
  ne?: string | null;
  eq?: string | null;
  le?: string | null;
  lt?: string | null;
  ge?: string | null;
  gt?: string | null;
  contains?: string | null;
  notContains?: string | null;
  between?: Array<string | null> | null;
  beginsWith?: string | null;
};

export type ModelARentTypeFilterInput = {
  eq?: ARentType | null;
  ne?: ARentType | null;
};

export type ModelFloatFilterInput = {
  ne?: number | null;
  eq?: number | null;
  le?: number | null;
  lt?: number | null;
  ge?: number | null;
  gt?: number | null;
  contains?: number | null;
  notContains?: number | null;
  between?: Array<number | null> | null;
};

export type ModelAImageFilterInput = {
  id?: ModelIDFilterInput | null;
  source?: ModelStringFilterInput | null;
  and?: Array<ModelAImageFilterInput | null> | null;
  or?: Array<ModelAImageFilterInput | null> | null;
  not?: ModelAImageFilterInput | null;
};

export type ModelADisasterFilterInput = {
  id?: ModelIDFilterInput | null;
  date?: ModelIntFilterInput | null;
  and?: Array<ModelADisasterFilterInput | null> | null;
  or?: Array<ModelADisasterFilterInput | null> | null;
  not?: ModelADisasterFilterInput | null;
};

export type ModelIntFilterInput = {
  ne?: number | null;
  eq?: number | null;
  le?: number | null;
  lt?: number | null;
  ge?: number | null;
  gt?: number | null;
  contains?: number | null;
  notContains?: number | null;
  between?: Array<number | null> | null;
};

export type ModelADisasterStatusFilterInput = {
  id?: ModelIDFilterInput | null;
  value?: ModelStringFilterInput | null;
  and?: Array<ModelADisasterStatusFilterInput | null> | null;
  or?: Array<ModelADisasterStatusFilterInput | null> | null;
  not?: ModelADisasterStatusFilterInput | null;
};

export type ModelADisasterTypeFilterInput = {
  id?: ModelIDFilterInput | null;
  value?: ModelStringFilterInput | null;
  and?: Array<ModelADisasterTypeFilterInput | null> | null;
  or?: Array<ModelADisasterTypeFilterInput | null> | null;
  not?: ModelADisasterTypeFilterInput | null;
};

export type ModelACommentFilterInput = {
  id?: ModelIDFilterInput | null;
  login?: ModelStringFilterInput | null;
  content?: ModelStringFilterInput | null;
  status?: ModelACommentStatusFilterInput | null;
  and?: Array<ModelACommentFilterInput | null> | null;
  or?: Array<ModelACommentFilterInput | null> | null;
  not?: ModelACommentFilterInput | null;
};

export type ModelACommentStatusFilterInput = {
  eq?: ACommentStatus | null;
  ne?: ACommentStatus | null;
};

export type CreateARentMutation = {
  __typename: "ARent";
  id: string;
  name: string;
  type: ARentType | null;
  price: number;
  description: string | null;
  thumbnail: {
    __typename: "AImage";
    id: string;
    source: string;
  } | null;
  disasters: {
    __typename: "ModelADisasterConnection";
    items: Array<{
      __typename: "ADisaster";
      id: string;
      date: number;
    } | null> | null;
    nextToken: string | null;
  } | null;
};

export type UpdateARentMutation = {
  __typename: "ARent";
  id: string;
  name: string;
  type: ARentType | null;
  price: number;
  description: string | null;
  thumbnail: {
    __typename: "AImage";
    id: string;
    source: string;
  } | null;
  disasters: {
    __typename: "ModelADisasterConnection";
    items: Array<{
      __typename: "ADisaster";
      id: string;
      date: number;
    } | null> | null;
    nextToken: string | null;
  } | null;
};

export type DeleteARentMutation = {
  __typename: "ARent";
  id: string;
  name: string;
  type: ARentType | null;
  price: number;
  description: string | null;
  thumbnail: {
    __typename: "AImage";
    id: string;
    source: string;
  } | null;
  disasters: {
    __typename: "ModelADisasterConnection";
    items: Array<{
      __typename: "ADisaster";
      id: string;
      date: number;
    } | null> | null;
    nextToken: string | null;
  } | null;
};

export type CreateAImageMutation = {
  __typename: "AImage";
  id: string;
  source: string;
};

export type UpdateAImageMutation = {
  __typename: "AImage";
  id: string;
  source: string;
};

export type DeleteAImageMutation = {
  __typename: "AImage";
  id: string;
  source: string;
};

export type CreateADisasterMutation = {
  __typename: "ADisaster";
  id: string;
  rent: {
    __typename: "ARent";
    id: string;
    name: string;
    type: ARentType | null;
    price: number;
    description: string | null;
    thumbnail: {
      __typename: "AImage";
      id: string;
      source: string;
    } | null;
    disasters: {
      __typename: "ModelADisasterConnection";
      nextToken: string | null;
    } | null;
  } | null;
  date: number;
  type: {
    __typename: "ADisasterType";
    id: string;
    value: string;
  } | null;
  status: {
    __typename: "ADisasterStatus";
    id: string;
    value: string;
    disaster: {
      __typename: "ADisaster";
      id: string;
      date: number;
    } | null;
  } | null;
  comments: {
    __typename: "ModelACommentConnection";
    items: Array<{
      __typename: "AComment";
      id: string;
      login: string | null;
      content: string | null;
      status: ACommentStatus | null;
    } | null> | null;
    nextToken: string | null;
  } | null;
};

export type UpdateADisasterMutation = {
  __typename: "ADisaster";
  id: string;
  rent: {
    __typename: "ARent";
    id: string;
    name: string;
    type: ARentType | null;
    price: number;
    description: string | null;
    thumbnail: {
      __typename: "AImage";
      id: string;
      source: string;
    } | null;
    disasters: {
      __typename: "ModelADisasterConnection";
      nextToken: string | null;
    } | null;
  } | null;
  date: number;
  type: {
    __typename: "ADisasterType";
    id: string;
    value: string;
  } | null;
  status: {
    __typename: "ADisasterStatus";
    id: string;
    value: string;
    disaster: {
      __typename: "ADisaster";
      id: string;
      date: number;
    } | null;
  } | null;
  comments: {
    __typename: "ModelACommentConnection";
    items: Array<{
      __typename: "AComment";
      id: string;
      login: string | null;
      content: string | null;
      status: ACommentStatus | null;
    } | null> | null;
    nextToken: string | null;
  } | null;
};

export type DeleteADisasterMutation = {
  __typename: "ADisaster";
  id: string;
  rent: {
    __typename: "ARent";
    id: string;
    name: string;
    type: ARentType | null;
    price: number;
    description: string | null;
    thumbnail: {
      __typename: "AImage";
      id: string;
      source: string;
    } | null;
    disasters: {
      __typename: "ModelADisasterConnection";
      nextToken: string | null;
    } | null;
  } | null;
  date: number;
  type: {
    __typename: "ADisasterType";
    id: string;
    value: string;
  } | null;
  status: {
    __typename: "ADisasterStatus";
    id: string;
    value: string;
    disaster: {
      __typename: "ADisaster";
      id: string;
      date: number;
    } | null;
  } | null;
  comments: {
    __typename: "ModelACommentConnection";
    items: Array<{
      __typename: "AComment";
      id: string;
      login: string | null;
      content: string | null;
      status: ACommentStatus | null;
    } | null> | null;
    nextToken: string | null;
  } | null;
};

export type CreateADisasterStatusMutation = {
  __typename: "ADisasterStatus";
  id: string;
  value: string;
  disaster: {
    __typename: "ADisaster";
    id: string;
    rent: {
      __typename: "ARent";
      id: string;
      name: string;
      type: ARentType | null;
      price: number;
      description: string | null;
    } | null;
    date: number;
    type: {
      __typename: "ADisasterType";
      id: string;
      value: string;
    } | null;
    status: {
      __typename: "ADisasterStatus";
      id: string;
      value: string;
    } | null;
    comments: {
      __typename: "ModelACommentConnection";
      nextToken: string | null;
    } | null;
  } | null;
};

export type UpdateADisasterStatusMutation = {
  __typename: "ADisasterStatus";
  id: string;
  value: string;
  disaster: {
    __typename: "ADisaster";
    id: string;
    rent: {
      __typename: "ARent";
      id: string;
      name: string;
      type: ARentType | null;
      price: number;
      description: string | null;
    } | null;
    date: number;
    type: {
      __typename: "ADisasterType";
      id: string;
      value: string;
    } | null;
    status: {
      __typename: "ADisasterStatus";
      id: string;
      value: string;
    } | null;
    comments: {
      __typename: "ModelACommentConnection";
      nextToken: string | null;
    } | null;
  } | null;
};

export type DeleteADisasterStatusMutation = {
  __typename: "ADisasterStatus";
  id: string;
  value: string;
  disaster: {
    __typename: "ADisaster";
    id: string;
    rent: {
      __typename: "ARent";
      id: string;
      name: string;
      type: ARentType | null;
      price: number;
      description: string | null;
    } | null;
    date: number;
    type: {
      __typename: "ADisasterType";
      id: string;
      value: string;
    } | null;
    status: {
      __typename: "ADisasterStatus";
      id: string;
      value: string;
    } | null;
    comments: {
      __typename: "ModelACommentConnection";
      nextToken: string | null;
    } | null;
  } | null;
};

export type CreateADisasterTypeMutation = {
  __typename: "ADisasterType";
  id: string;
  value: string;
};

export type UpdateADisasterTypeMutation = {
  __typename: "ADisasterType";
  id: string;
  value: string;
};

export type DeleteADisasterTypeMutation = {
  __typename: "ADisasterType";
  id: string;
  value: string;
};

export type CreateACommentMutation = {
  __typename: "AComment";
  id: string;
  login: string | null;
  content: string | null;
  disaster: {
    __typename: "ADisaster";
    id: string;
    rent: {
      __typename: "ARent";
      id: string;
      name: string;
      type: ARentType | null;
      price: number;
      description: string | null;
    } | null;
    date: number;
    type: {
      __typename: "ADisasterType";
      id: string;
      value: string;
    } | null;
    status: {
      __typename: "ADisasterStatus";
      id: string;
      value: string;
    } | null;
    comments: {
      __typename: "ModelACommentConnection";
      nextToken: string | null;
    } | null;
  } | null;
  images: Array<{
    __typename: "AImage";
    id: string;
    source: string;
  } | null> | null;
  status: ACommentStatus | null;
};

export type UpdateACommentMutation = {
  __typename: "AComment";
  id: string;
  login: string | null;
  content: string | null;
  disaster: {
    __typename: "ADisaster";
    id: string;
    rent: {
      __typename: "ARent";
      id: string;
      name: string;
      type: ARentType | null;
      price: number;
      description: string | null;
    } | null;
    date: number;
    type: {
      __typename: "ADisasterType";
      id: string;
      value: string;
    } | null;
    status: {
      __typename: "ADisasterStatus";
      id: string;
      value: string;
    } | null;
    comments: {
      __typename: "ModelACommentConnection";
      nextToken: string | null;
    } | null;
  } | null;
  images: Array<{
    __typename: "AImage";
    id: string;
    source: string;
  } | null> | null;
  status: ACommentStatus | null;
};

export type DeleteACommentMutation = {
  __typename: "AComment";
  id: string;
  login: string | null;
  content: string | null;
  disaster: {
    __typename: "ADisaster";
    id: string;
    rent: {
      __typename: "ARent";
      id: string;
      name: string;
      type: ARentType | null;
      price: number;
      description: string | null;
    } | null;
    date: number;
    type: {
      __typename: "ADisasterType";
      id: string;
      value: string;
    } | null;
    status: {
      __typename: "ADisasterStatus";
      id: string;
      value: string;
    } | null;
    comments: {
      __typename: "ModelACommentConnection";
      nextToken: string | null;
    } | null;
  } | null;
  images: Array<{
    __typename: "AImage";
    id: string;
    source: string;
  } | null> | null;
  status: ACommentStatus | null;
};

export type GetARentQuery = {
  __typename: "ARent";
  id: string;
  name: string;
  type: ARentType | null;
  price: number;
  description: string | null;
  thumbnail: {
    __typename: "AImage";
    id: string;
    source: string;
  } | null;
  disasters: {
    __typename: "ModelADisasterConnection";
    items: Array<{
      __typename: "ADisaster";
      id: string;
      date: number;
    } | null> | null;
    nextToken: string | null;
  } | null;
};

export type ListARentsQuery = {
  __typename: "ModelARentConnection";
  items: Array<{
    __typename: "ARent";
    id: string;
    name: string;
    type: ARentType | null;
    price: number;
    description: string | null;
    thumbnail: {
      __typename: "AImage";
      id: string;
      source: string;
    } | null;
    disasters: {
      __typename: "ModelADisasterConnection";
      nextToken: string | null;
    } | null;
  } | null> | null;
  nextToken: string | null;
};

export type GetAImageQuery = {
  __typename: "AImage";
  id: string;
  source: string;
};

export type ListAImagesQuery = {
  __typename: "ModelAImageConnection";
  items: Array<{
    __typename: "AImage";
    id: string;
    source: string;
  } | null> | null;
  nextToken: string | null;
};

export type GetADisasterQuery = {
  __typename: "ADisaster";
  id: string;
  rent: {
    __typename: "ARent";
    id: string;
    name: string;
    type: ARentType | null;
    price: number;
    description: string | null;
    thumbnail: {
      __typename: "AImage";
      id: string;
      source: string;
    } | null;
    disasters: {
      __typename: "ModelADisasterConnection";
      nextToken: string | null;
    } | null;
  } | null;
  date: number;
  type: {
    __typename: "ADisasterType";
    id: string;
    value: string;
  } | null;
  status: {
    __typename: "ADisasterStatus";
    id: string;
    value: string;
    disaster: {
      __typename: "ADisaster";
      id: string;
      date: number;
    } | null;
  } | null;
  comments: {
    __typename: "ModelACommentConnection";
    items: Array<{
      __typename: "AComment";
      id: string;
      login: string | null;
      content: string | null;
      status: ACommentStatus | null;
    } | null> | null;
    nextToken: string | null;
  } | null;
};

export type ListADisastersQuery = {
  __typename: "ModelADisasterConnection";
  items: Array<{
    __typename: "ADisaster";
    id: string;
    rent: {
      __typename: "ARent";
      id: string;
      name: string;
      type: ARentType | null;
      price: number;
      description: string | null;
    } | null;
    date: number;
    type: {
      __typename: "ADisasterType";
      id: string;
      value: string;
    } | null;
    status: {
      __typename: "ADisasterStatus";
      id: string;
      value: string;
    } | null;
    comments: {
      __typename: "ModelACommentConnection";
      nextToken: string | null;
    } | null;
  } | null> | null;
  nextToken: string | null;
};

export type GetADisasterStatusQuery = {
  __typename: "ADisasterStatus";
  id: string;
  value: string;
  disaster: {
    __typename: "ADisaster";
    id: string;
    rent: {
      __typename: "ARent";
      id: string;
      name: string;
      type: ARentType | null;
      price: number;
      description: string | null;
    } | null;
    date: number;
    type: {
      __typename: "ADisasterType";
      id: string;
      value: string;
    } | null;
    status: {
      __typename: "ADisasterStatus";
      id: string;
      value: string;
    } | null;
    comments: {
      __typename: "ModelACommentConnection";
      nextToken: string | null;
    } | null;
  } | null;
};

export type ListADisasterStatussQuery = {
  __typename: "ModelADisasterStatusConnection";
  items: Array<{
    __typename: "ADisasterStatus";
    id: string;
    value: string;
    disaster: {
      __typename: "ADisaster";
      id: string;
      date: number;
    } | null;
  } | null> | null;
  nextToken: string | null;
};

export type GetADisasterTypeQuery = {
  __typename: "ADisasterType";
  id: string;
  value: string;
};

export type ListADisasterTypesQuery = {
  __typename: "ModelADisasterTypeConnection";
  items: Array<{
    __typename: "ADisasterType";
    id: string;
    value: string;
  } | null> | null;
  nextToken: string | null;
};

export type GetACommentQuery = {
  __typename: "AComment";
  id: string;
  login: string | null;
  content: string | null;
  disaster: {
    __typename: "ADisaster";
    id: string;
    rent: {
      __typename: "ARent";
      id: string;
      name: string;
      type: ARentType | null;
      price: number;
      description: string | null;
    } | null;
    date: number;
    type: {
      __typename: "ADisasterType";
      id: string;
      value: string;
    } | null;
    status: {
      __typename: "ADisasterStatus";
      id: string;
      value: string;
    } | null;
    comments: {
      __typename: "ModelACommentConnection";
      nextToken: string | null;
    } | null;
  } | null;
  images: Array<{
    __typename: "AImage";
    id: string;
    source: string;
  } | null> | null;
  status: ACommentStatus | null;
};

export type ListACommentsQuery = {
  __typename: "ModelACommentConnection";
  items: Array<{
    __typename: "AComment";
    id: string;
    login: string | null;
    content: string | null;
    disaster: {
      __typename: "ADisaster";
      id: string;
      date: number;
    } | null;
    images: Array<{
      __typename: "AImage";
      id: string;
      source: string;
    } | null> | null;
    status: ACommentStatus | null;
  } | null> | null;
  nextToken: string | null;
};

export type OnCreateARentSubscription = {
  __typename: "ARent";
  id: string;
  name: string;
  type: ARentType | null;
  price: number;
  description: string | null;
  thumbnail: {
    __typename: "AImage";
    id: string;
    source: string;
  } | null;
  disasters: {
    __typename: "ModelADisasterConnection";
    items: Array<{
      __typename: "ADisaster";
      id: string;
      date: number;
    } | null> | null;
    nextToken: string | null;
  } | null;
};

export type OnUpdateARentSubscription = {
  __typename: "ARent";
  id: string;
  name: string;
  type: ARentType | null;
  price: number;
  description: string | null;
  thumbnail: {
    __typename: "AImage";
    id: string;
    source: string;
  } | null;
  disasters: {
    __typename: "ModelADisasterConnection";
    items: Array<{
      __typename: "ADisaster";
      id: string;
      date: number;
    } | null> | null;
    nextToken: string | null;
  } | null;
};

export type OnDeleteARentSubscription = {
  __typename: "ARent";
  id: string;
  name: string;
  type: ARentType | null;
  price: number;
  description: string | null;
  thumbnail: {
    __typename: "AImage";
    id: string;
    source: string;
  } | null;
  disasters: {
    __typename: "ModelADisasterConnection";
    items: Array<{
      __typename: "ADisaster";
      id: string;
      date: number;
    } | null> | null;
    nextToken: string | null;
  } | null;
};

export type OnCreateAImageSubscription = {
  __typename: "AImage";
  id: string;
  source: string;
};

export type OnUpdateAImageSubscription = {
  __typename: "AImage";
  id: string;
  source: string;
};

export type OnDeleteAImageSubscription = {
  __typename: "AImage";
  id: string;
  source: string;
};

export type OnCreateADisasterSubscription = {
  __typename: "ADisaster";
  id: string;
  rent: {
    __typename: "ARent";
    id: string;
    name: string;
    type: ARentType | null;
    price: number;
    description: string | null;
    thumbnail: {
      __typename: "AImage";
      id: string;
      source: string;
    } | null;
    disasters: {
      __typename: "ModelADisasterConnection";
      nextToken: string | null;
    } | null;
  } | null;
  date: number;
  type: {
    __typename: "ADisasterType";
    id: string;
    value: string;
  } | null;
  status: {
    __typename: "ADisasterStatus";
    id: string;
    value: string;
    disaster: {
      __typename: "ADisaster";
      id: string;
      date: number;
    } | null;
  } | null;
  comments: {
    __typename: "ModelACommentConnection";
    items: Array<{
      __typename: "AComment";
      id: string;
      login: string | null;
      content: string | null;
      status: ACommentStatus | null;
    } | null> | null;
    nextToken: string | null;
  } | null;
};

export type OnUpdateADisasterSubscription = {
  __typename: "ADisaster";
  id: string;
  rent: {
    __typename: "ARent";
    id: string;
    name: string;
    type: ARentType | null;
    price: number;
    description: string | null;
    thumbnail: {
      __typename: "AImage";
      id: string;
      source: string;
    } | null;
    disasters: {
      __typename: "ModelADisasterConnection";
      nextToken: string | null;
    } | null;
  } | null;
  date: number;
  type: {
    __typename: "ADisasterType";
    id: string;
    value: string;
  } | null;
  status: {
    __typename: "ADisasterStatus";
    id: string;
    value: string;
    disaster: {
      __typename: "ADisaster";
      id: string;
      date: number;
    } | null;
  } | null;
  comments: {
    __typename: "ModelACommentConnection";
    items: Array<{
      __typename: "AComment";
      id: string;
      login: string | null;
      content: string | null;
      status: ACommentStatus | null;
    } | null> | null;
    nextToken: string | null;
  } | null;
};

export type OnDeleteADisasterSubscription = {
  __typename: "ADisaster";
  id: string;
  rent: {
    __typename: "ARent";
    id: string;
    name: string;
    type: ARentType | null;
    price: number;
    description: string | null;
    thumbnail: {
      __typename: "AImage";
      id: string;
      source: string;
    } | null;
    disasters: {
      __typename: "ModelADisasterConnection";
      nextToken: string | null;
    } | null;
  } | null;
  date: number;
  type: {
    __typename: "ADisasterType";
    id: string;
    value: string;
  } | null;
  status: {
    __typename: "ADisasterStatus";
    id: string;
    value: string;
    disaster: {
      __typename: "ADisaster";
      id: string;
      date: number;
    } | null;
  } | null;
  comments: {
    __typename: "ModelACommentConnection";
    items: Array<{
      __typename: "AComment";
      id: string;
      login: string | null;
      content: string | null;
      status: ACommentStatus | null;
    } | null> | null;
    nextToken: string | null;
  } | null;
};

export type OnCreateADisasterStatusSubscription = {
  __typename: "ADisasterStatus";
  id: string;
  value: string;
  disaster: {
    __typename: "ADisaster";
    id: string;
    rent: {
      __typename: "ARent";
      id: string;
      name: string;
      type: ARentType | null;
      price: number;
      description: string | null;
    } | null;
    date: number;
    type: {
      __typename: "ADisasterType";
      id: string;
      value: string;
    } | null;
    status: {
      __typename: "ADisasterStatus";
      id: string;
      value: string;
    } | null;
    comments: {
      __typename: "ModelACommentConnection";
      nextToken: string | null;
    } | null;
  } | null;
};

export type OnUpdateADisasterStatusSubscription = {
  __typename: "ADisasterStatus";
  id: string;
  value: string;
  disaster: {
    __typename: "ADisaster";
    id: string;
    rent: {
      __typename: "ARent";
      id: string;
      name: string;
      type: ARentType | null;
      price: number;
      description: string | null;
    } | null;
    date: number;
    type: {
      __typename: "ADisasterType";
      id: string;
      value: string;
    } | null;
    status: {
      __typename: "ADisasterStatus";
      id: string;
      value: string;
    } | null;
    comments: {
      __typename: "ModelACommentConnection";
      nextToken: string | null;
    } | null;
  } | null;
};

export type OnDeleteADisasterStatusSubscription = {
  __typename: "ADisasterStatus";
  id: string;
  value: string;
  disaster: {
    __typename: "ADisaster";
    id: string;
    rent: {
      __typename: "ARent";
      id: string;
      name: string;
      type: ARentType | null;
      price: number;
      description: string | null;
    } | null;
    date: number;
    type: {
      __typename: "ADisasterType";
      id: string;
      value: string;
    } | null;
    status: {
      __typename: "ADisasterStatus";
      id: string;
      value: string;
    } | null;
    comments: {
      __typename: "ModelACommentConnection";
      nextToken: string | null;
    } | null;
  } | null;
};

export type OnCreateADisasterTypeSubscription = {
  __typename: "ADisasterType";
  id: string;
  value: string;
};

export type OnUpdateADisasterTypeSubscription = {
  __typename: "ADisasterType";
  id: string;
  value: string;
};

export type OnDeleteADisasterTypeSubscription = {
  __typename: "ADisasterType";
  id: string;
  value: string;
};

export type OnCreateACommentSubscription = {
  __typename: "AComment";
  id: string;
  login: string | null;
  content: string | null;
  disaster: {
    __typename: "ADisaster";
    id: string;
    rent: {
      __typename: "ARent";
      id: string;
      name: string;
      type: ARentType | null;
      price: number;
      description: string | null;
    } | null;
    date: number;
    type: {
      __typename: "ADisasterType";
      id: string;
      value: string;
    } | null;
    status: {
      __typename: "ADisasterStatus";
      id: string;
      value: string;
    } | null;
    comments: {
      __typename: "ModelACommentConnection";
      nextToken: string | null;
    } | null;
  } | null;
  images: Array<{
    __typename: "AImage";
    id: string;
    source: string;
  } | null> | null;
  status: ACommentStatus | null;
};

export type OnUpdateACommentSubscription = {
  __typename: "AComment";
  id: string;
  login: string | null;
  content: string | null;
  disaster: {
    __typename: "ADisaster";
    id: string;
    rent: {
      __typename: "ARent";
      id: string;
      name: string;
      type: ARentType | null;
      price: number;
      description: string | null;
    } | null;
    date: number;
    type: {
      __typename: "ADisasterType";
      id: string;
      value: string;
    } | null;
    status: {
      __typename: "ADisasterStatus";
      id: string;
      value: string;
    } | null;
    comments: {
      __typename: "ModelACommentConnection";
      nextToken: string | null;
    } | null;
  } | null;
  images: Array<{
    __typename: "AImage";
    id: string;
    source: string;
  } | null> | null;
  status: ACommentStatus | null;
};

export type OnDeleteACommentSubscription = {
  __typename: "AComment";
  id: string;
  login: string | null;
  content: string | null;
  disaster: {
    __typename: "ADisaster";
    id: string;
    rent: {
      __typename: "ARent";
      id: string;
      name: string;
      type: ARentType | null;
      price: number;
      description: string | null;
    } | null;
    date: number;
    type: {
      __typename: "ADisasterType";
      id: string;
      value: string;
    } | null;
    status: {
      __typename: "ADisasterStatus";
      id: string;
      value: string;
    } | null;
    comments: {
      __typename: "ModelACommentConnection";
      nextToken: string | null;
    } | null;
  } | null;
  images: Array<{
    __typename: "AImage";
    id: string;
    source: string;
  } | null> | null;
  status: ACommentStatus | null;
};

@Injectable({
  providedIn: "root"
})
export class APIService {
  async CreateARent(input: CreateARentInput): Promise<CreateARentMutation> {
    const statement = `mutation CreateARent($input: CreateARentInput!) {
        createARent(input: $input) {
          __typename
          id
          name
          type
          price
          description
          thumbnail {
            __typename
            id
            source
          }
          disasters {
            __typename
            items {
              __typename
              id
              date
            }
            nextToken
          }
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <CreateARentMutation>response.data.createARent;
  }
  async UpdateARent(input: UpdateARentInput): Promise<UpdateARentMutation> {
    const statement = `mutation UpdateARent($input: UpdateARentInput!) {
        updateARent(input: $input) {
          __typename
          id
          name
          type
          price
          description
          thumbnail {
            __typename
            id
            source
          }
          disasters {
            __typename
            items {
              __typename
              id
              date
            }
            nextToken
          }
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <UpdateARentMutation>response.data.updateARent;
  }
  async DeleteARent(input: DeleteARentInput): Promise<DeleteARentMutation> {
    const statement = `mutation DeleteARent($input: DeleteARentInput!) {
        deleteARent(input: $input) {
          __typename
          id
          name
          type
          price
          description
          thumbnail {
            __typename
            id
            source
          }
          disasters {
            __typename
            items {
              __typename
              id
              date
            }
            nextToken
          }
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <DeleteARentMutation>response.data.deleteARent;
  }
  async CreateAImage(input: CreateAImageInput): Promise<CreateAImageMutation> {
    const statement = `mutation CreateAImage($input: CreateAImageInput!) {
        createAImage(input: $input) {
          __typename
          id
          source
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <CreateAImageMutation>response.data.createAImage;
  }
  async UpdateAImage(input: UpdateAImageInput): Promise<UpdateAImageMutation> {
    const statement = `mutation UpdateAImage($input: UpdateAImageInput!) {
        updateAImage(input: $input) {
          __typename
          id
          source
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <UpdateAImageMutation>response.data.updateAImage;
  }
  async DeleteAImage(input: DeleteAImageInput): Promise<DeleteAImageMutation> {
    const statement = `mutation DeleteAImage($input: DeleteAImageInput!) {
        deleteAImage(input: $input) {
          __typename
          id
          source
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <DeleteAImageMutation>response.data.deleteAImage;
  }
  async CreateADisaster(
    input: CreateADisasterInput
  ): Promise<CreateADisasterMutation> {
    const statement = `mutation CreateADisaster($input: CreateADisasterInput!) {
        createADisaster(input: $input) {
          __typename
          id
          rent {
            __typename
            id
            name
            type
            price
            description
            thumbnail {
              __typename
              id
              source
            }
            disasters {
              __typename
              nextToken
            }
          }
          date
          type {
            __typename
            id
            value
          }
          status {
            __typename
            id
            value
            disaster {
              __typename
              id
              date
            }
          }
          comments {
            __typename
            items {
              __typename
              id
              login
              content
              status
            }
            nextToken
          }
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <CreateADisasterMutation>response.data.createADisaster;
  }
  async UpdateADisaster(
    input: UpdateADisasterInput
  ): Promise<UpdateADisasterMutation> {
    const statement = `mutation UpdateADisaster($input: UpdateADisasterInput!) {
        updateADisaster(input: $input) {
          __typename
          id
          rent {
            __typename
            id
            name
            type
            price
            description
            thumbnail {
              __typename
              id
              source
            }
            disasters {
              __typename
              nextToken
            }
          }
          date
          type {
            __typename
            id
            value
          }
          status {
            __typename
            id
            value
            disaster {
              __typename
              id
              date
            }
          }
          comments {
            __typename
            items {
              __typename
              id
              login
              content
              status
            }
            nextToken
          }
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <UpdateADisasterMutation>response.data.updateADisaster;
  }
  async DeleteADisaster(
    input: DeleteADisasterInput
  ): Promise<DeleteADisasterMutation> {
    const statement = `mutation DeleteADisaster($input: DeleteADisasterInput!) {
        deleteADisaster(input: $input) {
          __typename
          id
          rent {
            __typename
            id
            name
            type
            price
            description
            thumbnail {
              __typename
              id
              source
            }
            disasters {
              __typename
              nextToken
            }
          }
          date
          type {
            __typename
            id
            value
          }
          status {
            __typename
            id
            value
            disaster {
              __typename
              id
              date
            }
          }
          comments {
            __typename
            items {
              __typename
              id
              login
              content
              status
            }
            nextToken
          }
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <DeleteADisasterMutation>response.data.deleteADisaster;
  }
  async CreateADisasterStatus(
    input: CreateADisasterStatusInput
  ): Promise<CreateADisasterStatusMutation> {
    const statement = `mutation CreateADisasterStatus($input: CreateADisasterStatusInput!) {
        createADisasterStatus(input: $input) {
          __typename
          id
          value
          disaster {
            __typename
            id
            rent {
              __typename
              id
              name
              type
              price
              description
            }
            date
            type {
              __typename
              id
              value
            }
            status {
              __typename
              id
              value
            }
            comments {
              __typename
              nextToken
            }
          }
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <CreateADisasterStatusMutation>response.data.createADisasterStatus;
  }
  async UpdateADisasterStatus(
    input: UpdateADisasterStatusInput
  ): Promise<UpdateADisasterStatusMutation> {
    const statement = `mutation UpdateADisasterStatus($input: UpdateADisasterStatusInput!) {
        updateADisasterStatus(input: $input) {
          __typename
          id
          value
          disaster {
            __typename
            id
            rent {
              __typename
              id
              name
              type
              price
              description
            }
            date
            type {
              __typename
              id
              value
            }
            status {
              __typename
              id
              value
            }
            comments {
              __typename
              nextToken
            }
          }
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <UpdateADisasterStatusMutation>response.data.updateADisasterStatus;
  }
  async DeleteADisasterStatus(
    input: DeleteADisasterStatusInput
  ): Promise<DeleteADisasterStatusMutation> {
    const statement = `mutation DeleteADisasterStatus($input: DeleteADisasterStatusInput!) {
        deleteADisasterStatus(input: $input) {
          __typename
          id
          value
          disaster {
            __typename
            id
            rent {
              __typename
              id
              name
              type
              price
              description
            }
            date
            type {
              __typename
              id
              value
            }
            status {
              __typename
              id
              value
            }
            comments {
              __typename
              nextToken
            }
          }
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <DeleteADisasterStatusMutation>response.data.deleteADisasterStatus;
  }
  async CreateADisasterType(
    input: CreateADisasterTypeInput
  ): Promise<CreateADisasterTypeMutation> {
    const statement = `mutation CreateADisasterType($input: CreateADisasterTypeInput!) {
        createADisasterType(input: $input) {
          __typename
          id
          value
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <CreateADisasterTypeMutation>response.data.createADisasterType;
  }
  async UpdateADisasterType(
    input: UpdateADisasterTypeInput
  ): Promise<UpdateADisasterTypeMutation> {
    const statement = `mutation UpdateADisasterType($input: UpdateADisasterTypeInput!) {
        updateADisasterType(input: $input) {
          __typename
          id
          value
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <UpdateADisasterTypeMutation>response.data.updateADisasterType;
  }
  async DeleteADisasterType(
    input: DeleteADisasterTypeInput
  ): Promise<DeleteADisasterTypeMutation> {
    const statement = `mutation DeleteADisasterType($input: DeleteADisasterTypeInput!) {
        deleteADisasterType(input: $input) {
          __typename
          id
          value
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <DeleteADisasterTypeMutation>response.data.deleteADisasterType;
  }
  async CreateAComment(
    input: CreateACommentInput
  ): Promise<CreateACommentMutation> {
    const statement = `mutation CreateAComment($input: CreateACommentInput!) {
        createAComment(input: $input) {
          __typename
          id
          login
          content
          disaster {
            __typename
            id
            rent {
              __typename
              id
              name
              type
              price
              description
            }
            date
            type {
              __typename
              id
              value
            }
            status {
              __typename
              id
              value
            }
            comments {
              __typename
              nextToken
            }
          }
          images {
            __typename
            id
            source
          }
          status
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <CreateACommentMutation>response.data.createAComment;
  }
  async UpdateAComment(
    input: UpdateACommentInput
  ): Promise<UpdateACommentMutation> {
    const statement = `mutation UpdateAComment($input: UpdateACommentInput!) {
        updateAComment(input: $input) {
          __typename
          id
          login
          content
          disaster {
            __typename
            id
            rent {
              __typename
              id
              name
              type
              price
              description
            }
            date
            type {
              __typename
              id
              value
            }
            status {
              __typename
              id
              value
            }
            comments {
              __typename
              nextToken
            }
          }
          images {
            __typename
            id
            source
          }
          status
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <UpdateACommentMutation>response.data.updateAComment;
  }
  async DeleteAComment(
    input: DeleteACommentInput
  ): Promise<DeleteACommentMutation> {
    const statement = `mutation DeleteAComment($input: DeleteACommentInput!) {
        deleteAComment(input: $input) {
          __typename
          id
          login
          content
          disaster {
            __typename
            id
            rent {
              __typename
              id
              name
              type
              price
              description
            }
            date
            type {
              __typename
              id
              value
            }
            status {
              __typename
              id
              value
            }
            comments {
              __typename
              nextToken
            }
          }
          images {
            __typename
            id
            source
          }
          status
        }
      }`;
    const gqlAPIServiceArguments: any = {
      input
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <DeleteACommentMutation>response.data.deleteAComment;
  }
  async GetARent(id: string): Promise<GetARentQuery> {
    const statement = `query GetARent($id: ID!) {
        getARent(id: $id) {
          __typename
          id
          name
          type
          price
          description
          thumbnail {
            __typename
            id
            source
          }
          disasters {
            __typename
            items {
              __typename
              id
              date
            }
            nextToken
          }
        }
      }`;
    const gqlAPIServiceArguments: any = {
      id
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <GetARentQuery>response.data.getARent;
  }
  async ListARents(
    filter?: ModelARentFilterInput,
    limit?: number,
    nextToken?: string
  ): Promise<ListARentsQuery> {
    const statement = `query ListARents($filter: ModelARentFilterInput, $limit: Int, $nextToken: String) {
        listARents(filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            name
            type
            price
            description
            thumbnail {
              __typename
              id
              source
            }
            disasters {
              __typename
              nextToken
            }
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <ListARentsQuery>response.data.listARents;
  }
  async GetAImage(id: string): Promise<GetAImageQuery> {
    const statement = `query GetAImage($id: ID!) {
        getAImage(id: $id) {
          __typename
          id
          source
        }
      }`;
    const gqlAPIServiceArguments: any = {
      id
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <GetAImageQuery>response.data.getAImage;
  }
  async ListAImages(
    filter?: ModelAImageFilterInput,
    limit?: number,
    nextToken?: string
  ): Promise<ListAImagesQuery> {
    const statement = `query ListAImages($filter: ModelAImageFilterInput, $limit: Int, $nextToken: String) {
        listAImages(filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            source
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <ListAImagesQuery>response.data.listAImages;
  }
  async GetADisaster(id: string): Promise<GetADisasterQuery> {
    const statement = `query GetADisaster($id: ID!) {
        getADisaster(id: $id) {
          __typename
          id
          rent {
            __typename
            id
            name
            type
            price
            description
            thumbnail {
              __typename
              id
              source
            }
            disasters {
              __typename
              nextToken
            }
          }
          date
          type {
            __typename
            id
            value
          }
          status {
            __typename
            id
            value
            disaster {
              __typename
              id
              date
            }
          }
          comments {
            __typename
            items {
              __typename
              id
              login
              content
              status
            }
            nextToken
          }
        }
      }`;
    const gqlAPIServiceArguments: any = {
      id
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <GetADisasterQuery>response.data.getADisaster;
  }
  async ListADisasters(
    filter?: ModelADisasterFilterInput,
    limit?: number,
    nextToken?: string
  ): Promise<ListADisastersQuery> {
    const statement = `query ListADisasters($filter: ModelADisasterFilterInput, $limit: Int, $nextToken: String) {
        listADisasters(filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            rent {
              __typename
              id
              name
              type
              price
              description
            }
            date
            type {
              __typename
              id
              value
            }
            status {
              __typename
              id
              value
            }
            comments {
              __typename
              nextToken
            }
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <ListADisastersQuery>response.data.listADisasters;
  }
  async GetADisasterStatus(id: string): Promise<GetADisasterStatusQuery> {
    const statement = `query GetADisasterStatus($id: ID!) {
        getADisasterStatus(id: $id) {
          __typename
          id
          value
          disaster {
            __typename
            id
            rent {
              __typename
              id
              name
              type
              price
              description
            }
            date
            type {
              __typename
              id
              value
            }
            status {
              __typename
              id
              value
            }
            comments {
              __typename
              nextToken
            }
          }
        }
      }`;
    const gqlAPIServiceArguments: any = {
      id
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <GetADisasterStatusQuery>response.data.getADisasterStatus;
  }
  async ListADisasterStatuss(
    filter?: ModelADisasterStatusFilterInput,
    limit?: number,
    nextToken?: string
  ): Promise<ListADisasterStatussQuery> {
    const statement = `query ListADisasterStatuss($filter: ModelADisasterStatusFilterInput, $limit: Int, $nextToken: String) {
        listADisasterStatuss(filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            value
            disaster {
              __typename
              id
              date
            }
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <ListADisasterStatussQuery>response.data.listADisasterStatuss;
  }
  async GetADisasterType(id: string): Promise<GetADisasterTypeQuery> {
    const statement = `query GetADisasterType($id: ID!) {
        getADisasterType(id: $id) {
          __typename
          id
          value
        }
      }`;
    const gqlAPIServiceArguments: any = {
      id
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <GetADisasterTypeQuery>response.data.getADisasterType;
  }
  async ListADisasterTypes(
    filter?: ModelADisasterTypeFilterInput,
    limit?: number,
    nextToken?: string
  ): Promise<ListADisasterTypesQuery> {
    const statement = `query ListADisasterTypes($filter: ModelADisasterTypeFilterInput, $limit: Int, $nextToken: String) {
        listADisasterTypes(filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            value
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <ListADisasterTypesQuery>response.data.listADisasterTypes;
  }
  async GetAComment(id: string): Promise<GetACommentQuery> {
    const statement = `query GetAComment($id: ID!) {
        getAComment(id: $id) {
          __typename
          id
          login
          content
          disaster {
            __typename
            id
            rent {
              __typename
              id
              name
              type
              price
              description
            }
            date
            type {
              __typename
              id
              value
            }
            status {
              __typename
              id
              value
            }
            comments {
              __typename
              nextToken
            }
          }
          images {
            __typename
            id
            source
          }
          status
        }
      }`;
    const gqlAPIServiceArguments: any = {
      id
    };
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <GetACommentQuery>response.data.getAComment;
  }
  async ListAComments(
    filter?: ModelACommentFilterInput,
    limit?: number,
    nextToken?: string
  ): Promise<ListACommentsQuery> {
    const statement = `query ListAComments($filter: ModelACommentFilterInput, $limit: Int, $nextToken: String) {
        listAComments(filter: $filter, limit: $limit, nextToken: $nextToken) {
          __typename
          items {
            __typename
            id
            login
            content
            disaster {
              __typename
              id
              date
            }
            images {
              __typename
              id
              source
            }
            status
          }
          nextToken
        }
      }`;
    const gqlAPIServiceArguments: any = {};
    if (filter) {
      gqlAPIServiceArguments.filter = filter;
    }
    if (limit) {
      gqlAPIServiceArguments.limit = limit;
    }
    if (nextToken) {
      gqlAPIServiceArguments.nextToken = nextToken;
    }
    const response = (await API.graphql(
      graphqlOperation(statement, gqlAPIServiceArguments)
    )) as any;
    return <ListACommentsQuery>response.data.listAComments;
  }
  OnCreateARentListener: Observable<OnCreateARentSubscription> = API.graphql(
    graphqlOperation(
      `subscription OnCreateARent {
        onCreateARent {
          __typename
          id
          name
          type
          price
          description
          thumbnail {
            __typename
            id
            source
          }
          disasters {
            __typename
            items {
              __typename
              id
              date
            }
            nextToken
          }
        }
      }`
    )
  ) as Observable<OnCreateARentSubscription>;

  OnUpdateARentListener: Observable<OnUpdateARentSubscription> = API.graphql(
    graphqlOperation(
      `subscription OnUpdateARent {
        onUpdateARent {
          __typename
          id
          name
          type
          price
          description
          thumbnail {
            __typename
            id
            source
          }
          disasters {
            __typename
            items {
              __typename
              id
              date
            }
            nextToken
          }
        }
      }`
    )
  ) as Observable<OnUpdateARentSubscription>;

  OnDeleteARentListener: Observable<OnDeleteARentSubscription> = API.graphql(
    graphqlOperation(
      `subscription OnDeleteARent {
        onDeleteARent {
          __typename
          id
          name
          type
          price
          description
          thumbnail {
            __typename
            id
            source
          }
          disasters {
            __typename
            items {
              __typename
              id
              date
            }
            nextToken
          }
        }
      }`
    )
  ) as Observable<OnDeleteARentSubscription>;

  OnCreateAImageListener: Observable<OnCreateAImageSubscription> = API.graphql(
    graphqlOperation(
      `subscription OnCreateAImage {
        onCreateAImage {
          __typename
          id
          source
        }
      }`
    )
  ) as Observable<OnCreateAImageSubscription>;

  OnUpdateAImageListener: Observable<OnUpdateAImageSubscription> = API.graphql(
    graphqlOperation(
      `subscription OnUpdateAImage {
        onUpdateAImage {
          __typename
          id
          source
        }
      }`
    )
  ) as Observable<OnUpdateAImageSubscription>;

  OnDeleteAImageListener: Observable<OnDeleteAImageSubscription> = API.graphql(
    graphqlOperation(
      `subscription OnDeleteAImage {
        onDeleteAImage {
          __typename
          id
          source
        }
      }`
    )
  ) as Observable<OnDeleteAImageSubscription>;

  OnCreateADisasterListener: Observable<
    OnCreateADisasterSubscription
  > = API.graphql(
    graphqlOperation(
      `subscription OnCreateADisaster {
        onCreateADisaster {
          __typename
          id
          rent {
            __typename
            id
            name
            type
            price
            description
            thumbnail {
              __typename
              id
              source
            }
            disasters {
              __typename
              nextToken
            }
          }
          date
          type {
            __typename
            id
            value
          }
          status {
            __typename
            id
            value
            disaster {
              __typename
              id
              date
            }
          }
          comments {
            __typename
            items {
              __typename
              id
              login
              content
              status
            }
            nextToken
          }
        }
      }`
    )
  ) as Observable<OnCreateADisasterSubscription>;

  OnUpdateADisasterListener: Observable<
    OnUpdateADisasterSubscription
  > = API.graphql(
    graphqlOperation(
      `subscription OnUpdateADisaster {
        onUpdateADisaster {
          __typename
          id
          rent {
            __typename
            id
            name
            type
            price
            description
            thumbnail {
              __typename
              id
              source
            }
            disasters {
              __typename
              nextToken
            }
          }
          date
          type {
            __typename
            id
            value
          }
          status {
            __typename
            id
            value
            disaster {
              __typename
              id
              date
            }
          }
          comments {
            __typename
            items {
              __typename
              id
              login
              content
              status
            }
            nextToken
          }
        }
      }`
    )
  ) as Observable<OnUpdateADisasterSubscription>;

  OnDeleteADisasterListener: Observable<
    OnDeleteADisasterSubscription
  > = API.graphql(
    graphqlOperation(
      `subscription OnDeleteADisaster {
        onDeleteADisaster {
          __typename
          id
          rent {
            __typename
            id
            name
            type
            price
            description
            thumbnail {
              __typename
              id
              source
            }
            disasters {
              __typename
              nextToken
            }
          }
          date
          type {
            __typename
            id
            value
          }
          status {
            __typename
            id
            value
            disaster {
              __typename
              id
              date
            }
          }
          comments {
            __typename
            items {
              __typename
              id
              login
              content
              status
            }
            nextToken
          }
        }
      }`
    )
  ) as Observable<OnDeleteADisasterSubscription>;

  OnCreateADisasterStatusListener: Observable<
    OnCreateADisasterStatusSubscription
  > = API.graphql(
    graphqlOperation(
      `subscription OnCreateADisasterStatus {
        onCreateADisasterStatus {
          __typename
          id
          value
          disaster {
            __typename
            id
            rent {
              __typename
              id
              name
              type
              price
              description
            }
            date
            type {
              __typename
              id
              value
            }
            status {
              __typename
              id
              value
            }
            comments {
              __typename
              nextToken
            }
          }
        }
      }`
    )
  ) as Observable<OnCreateADisasterStatusSubscription>;

  OnUpdateADisasterStatusListener: Observable<
    OnUpdateADisasterStatusSubscription
  > = API.graphql(
    graphqlOperation(
      `subscription OnUpdateADisasterStatus {
        onUpdateADisasterStatus {
          __typename
          id
          value
          disaster {
            __typename
            id
            rent {
              __typename
              id
              name
              type
              price
              description
            }
            date
            type {
              __typename
              id
              value
            }
            status {
              __typename
              id
              value
            }
            comments {
              __typename
              nextToken
            }
          }
        }
      }`
    )
  ) as Observable<OnUpdateADisasterStatusSubscription>;

  OnDeleteADisasterStatusListener: Observable<
    OnDeleteADisasterStatusSubscription
  > = API.graphql(
    graphqlOperation(
      `subscription OnDeleteADisasterStatus {
        onDeleteADisasterStatus {
          __typename
          id
          value
          disaster {
            __typename
            id
            rent {
              __typename
              id
              name
              type
              price
              description
            }
            date
            type {
              __typename
              id
              value
            }
            status {
              __typename
              id
              value
            }
            comments {
              __typename
              nextToken
            }
          }
        }
      }`
    )
  ) as Observable<OnDeleteADisasterStatusSubscription>;

  OnCreateADisasterTypeListener: Observable<
    OnCreateADisasterTypeSubscription
  > = API.graphql(
    graphqlOperation(
      `subscription OnCreateADisasterType {
        onCreateADisasterType {
          __typename
          id
          value
        }
      }`
    )
  ) as Observable<OnCreateADisasterTypeSubscription>;

  OnUpdateADisasterTypeListener: Observable<
    OnUpdateADisasterTypeSubscription
  > = API.graphql(
    graphqlOperation(
      `subscription OnUpdateADisasterType {
        onUpdateADisasterType {
          __typename
          id
          value
        }
      }`
    )
  ) as Observable<OnUpdateADisasterTypeSubscription>;

  OnDeleteADisasterTypeListener: Observable<
    OnDeleteADisasterTypeSubscription
  > = API.graphql(
    graphqlOperation(
      `subscription OnDeleteADisasterType {
        onDeleteADisasterType {
          __typename
          id
          value
        }
      }`
    )
  ) as Observable<OnDeleteADisasterTypeSubscription>;

  OnCreateACommentListener: Observable<
    OnCreateACommentSubscription
  > = API.graphql(
    graphqlOperation(
      `subscription OnCreateAComment {
        onCreateAComment {
          __typename
          id
          login
          content
          disaster {
            __typename
            id
            rent {
              __typename
              id
              name
              type
              price
              description
            }
            date
            type {
              __typename
              id
              value
            }
            status {
              __typename
              id
              value
            }
            comments {
              __typename
              nextToken
            }
          }
          images {
            __typename
            id
            source
          }
          status
        }
      }`
    )
  ) as Observable<OnCreateACommentSubscription>;

  OnUpdateACommentListener: Observable<
    OnUpdateACommentSubscription
  > = API.graphql(
    graphqlOperation(
      `subscription OnUpdateAComment {
        onUpdateAComment {
          __typename
          id
          login
          content
          disaster {
            __typename
            id
            rent {
              __typename
              id
              name
              type
              price
              description
            }
            date
            type {
              __typename
              id
              value
            }
            status {
              __typename
              id
              value
            }
            comments {
              __typename
              nextToken
            }
          }
          images {
            __typename
            id
            source
          }
          status
        }
      }`
    )
  ) as Observable<OnUpdateACommentSubscription>;

  OnDeleteACommentListener: Observable<
    OnDeleteACommentSubscription
  > = API.graphql(
    graphqlOperation(
      `subscription OnDeleteAComment {
        onDeleteAComment {
          __typename
          id
          login
          content
          disaster {
            __typename
            id
            rent {
              __typename
              id
              name
              type
              price
              description
            }
            date
            type {
              __typename
              id
              value
            }
            status {
              __typename
              id
              value
            }
            comments {
              __typename
              nextToken
            }
          }
          images {
            __typename
            id
            source
          }
          status
        }
      }`
    )
  ) as Observable<OnDeleteACommentSubscription>;
}
