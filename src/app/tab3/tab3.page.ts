import {Component} from '@angular/core';
import {EventsServices,AppEvents} from './../modules/commons/services/events.services';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  /**************************************************************************
  * CONSTRUCTORS
  **************************************************************************/
  constructor(private eventsService   : EventsServices) {}


  /**************************************************************************
  * API
  **************************************************************************/
  mockOffline(event:any){
    if(event.detail.checked){
      this.eventsService.fireEvent(AppEvents.ON_ONLINE, {type:"mock network",downlinkMax:100});
    }else{
      this.eventsService.fireEvent(AppEvents.ON_OFFLINE);
    }
  }


  get online() : boolean{
    return this.eventsService.isOnline;
  }
}
