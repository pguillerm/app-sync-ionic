import { IonicModule }          from '@ionic/angular';
import { RouterModule }         from '@angular/router';
import { NgModule }             from '@angular/core';
import { CommonModule }         from '@angular/common';
import { FormsModule }          from '@angular/forms';
import { Tab3Page }             from './tab3.page';
import {CommonsModule}          from './../modules/commons/commons.module';
@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    CommonsModule
  ],
  declarations: [Tab3Page],
  exports:[Tab3Page]
})
export class Tab3PageModule {}
