import {Component, OnInit}        from '@angular/core';
import {Router}                   from '@angular/router';
import {IconDefinition}           from '@fortawesome/fontawesome-common-types';
import {AmplifyService}           from  'aws-amplify-angular';

import {isNull}                   from './../modules/commons/tools/checks';
import {DaoFactory,Dao}           from './../modules/commons/services/dao/dao.factory';
import {Rent,RentType}            from './../modules/commons/models/commons.models.interfaces';
import {Icons,getIcon}            from './../modules/commons/models/icons';

const BASE_URL = "/tab1";

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit{
  /**************************************************************************
  * ATTRIBUTES
  **************************************************************************/
  private dao     : Dao;
  rents           : Rent[];


  /**************************************************************************
  * CONSTRUCTORS
  **************************************************************************/
  constructor(private daoFactory:DaoFactory,
              private router: Router,
              private amplify: AmplifyService) {
    this.amplify.auth().currentAuthenticatedUser().then(console.log)
    this.dao = this.daoFactory.getDao();
  }


  ngOnInit(){
    this.dao.findAllRents()
            .then((data)=>this.rents=data);
  }


  resolveIcon(rent:Rent):IconDefinition{
    let type = RentType[rent.type];
    let result = isNull(type)?null:getIcon(type.toLowerCase());
    return result==null ? Icons.flat:result;
  }

  navigateToDetail(index : string){
    this.router.navigate([`${BASE_URL}/${index}`]);
  }

}
