import {Component,Input}          from '@angular/core';
import {IconDefinition}           from '@fortawesome/fontawesome-common-types';
import {Icons,getIcon}            from './../../../modules/commons/models/icons';
import {normalize}                from './../../../modules/commons/services/tools';
import {Disaster,DisasterStatus}  from './../../../modules/commons/models/commons.models.interfaces';

const DISASTER_ICONS = {
    "plomberie"   : Icons.plumbing,
    "electricite" : Icons.electricity,
    "chauffage"   : Icons.heating
};

@Component({
    selector      : 'disaster-display',
    templateUrl   : './disaster.display.html',
    styleUrls     : ['./disaster.display.scss']
  })
  export class DisasterDisplay {
    @Input() value : Disaster;

    constructor(){
    }


    get icon():IconDefinition{
        let result = Icons.defaultDisasterIcon;

        if(this.value != null && this.value.type != null){
          let key = normalize(this.value.type.id).toLowerCase();
          let icon = DISASTER_ICONS[key];
          if(icon != null){
            result = icon;
          }
        }
        return result;
    }

    get iconStatus():IconDefinition{
      let result = null;

      if(this.value != null && this.value.status != null){
        let type = DisasterStatus[this.value.status];
        result = getIcon(type);
      }

      if(result == null){
        result = Icons.defaultDisasterIcon;
      }
      return result;
    }
  }