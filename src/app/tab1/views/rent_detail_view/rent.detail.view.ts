import {Component, OnInit}              from '@angular/core';
import {ActivatedRoute}                 from '@angular/router';
import {DaoFactory,Dao}                 from './../../../modules/commons/services/dao/dao.factory';
import {Rent,Disaster}                  from './../../../modules/commons/models/commons.models.interfaces';
import { isNotNull } from 'src/app/modules/commons/tools/checks';

@Component({
    templateUrl   : './rent.detail.view.html',
    styleUrls     : ['./rent.detail.view.scss'],
    providers     : []
})
export class RentDetailView implements OnInit{
    /***************************************************************************
    * ATTRIBUTE
    ***************************************************************************/
    private dao     : Dao;
    title           : string;
    uid             : string;
    rent            : Rent;
    disasters       : Disaster[];
    /***************************************************************************
    * CONSTRUCTORS
    ***************************************************************************/
    constructor(private route       : ActivatedRoute,
                private daoFactory  : DaoFactory) {
        this.dao = this.daoFactory.getDao();
    }


    ngOnInit(): void {
        this.route.paramMap.subscribe((params)=>{
            let uid = params.get("uid");
            this.loadData(uid);
        });
    }

    loadData(uid:string){
        this.uid = uid;
        this.dao.findRent(this.uid)
                .then(data => {
                    if(isNotNull(data)){
                        this.rent = data;
                        this.title = data.name;
                        this.disasters = data.disasters;
                    }
                });
    }
    


    /***************************************************************************
    * GETTER
    ***************************************************************************/
    get thumbnail() : string{
        let result = null;
        if(this.rent!=null && 
           this.rent.thumbnail !=null){
            result = this.rent.thumbnail.source;
        }
        return result;
    }
}