import {IonicModule}                      from '@ionic/angular';
import {RouterModule}                     from '@angular/router';
import {NgModule}                         from '@angular/core';
import {CommonModule}                     from '@angular/common';
import {FormsModule}                      from '@angular/forms';
import {FontAwesomeModule}                from '@fortawesome/angular-fontawesome';

import {AmplifyAngularModule}             from  'aws-amplify-angular';

import {CommonsModule}                    from './../modules/commons/commons.module'
import {Tab1Page}                         from './tab1.page';
import {RentDetailView}                   from './views/rent_detail_view/rent.detail.view';
import {DisasterDisplay}                  from './components/disaster_display/disaster.display';

@NgModule({
  imports: [
    IonicModule,
    FontAwesomeModule,
    CommonModule,
    FormsModule,
    CommonsModule,
    AmplifyAngularModule
  ],
  declarations: [
    Tab1Page,
    RentDetailView,
    DisasterDisplay
  ],
  exports:[
    Tab1Page,
    RentDetailView
  ]
})
export class Tab1PageModule {}
