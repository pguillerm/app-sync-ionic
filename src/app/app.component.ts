import {Component}                    from '@angular/core';
import {Platform}                     from '@ionic/angular';
import {SplashScreen}                 from '@ionic-native/splash-screen/ngx';
import {StatusBar}                    from '@ionic-native/status-bar/ngx';
import {Network}                      from '@ionic-native/network/ngx';
import {ToastController}              from '@ionic/angular';
import {EventsServices,AppEvents}     from './modules/commons/services/events.services';
import {AppSyncService}               from './modules/commons/services/dao/app.sync.service';



@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private network:Network,
    private eventsService:EventsServices,
    private appSyncClient : AppSyncService,
    private toastController: ToastController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then((source) => {
      console.log("platform source " +  JSON.stringify(source));
      
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.eventsService.isOnline= this.isOnLine();
      
      this.platform.pause.subscribe((event)=>{
         console.log("pause");
        this.eventsService.fireEvent(AppEvents.PAUSE);
      });
      
      this.platform.resume.subscribe((event)=>{
        console.log("resume");
        this.eventsService.fireEvent(AppEvents.RESUME);
      });
     
      this.network.onConnect().subscribe((event)=>{
        console.log("online");
        this.eventsService.fireEvent(AppEvents.ON_ONLINE, 
                                    {type:this.network.type,downlinkMax:this.network.downlinkMax});
      });

      this.network.onDisconnect().subscribe((event)=>{
        console.log("offline");
        this.eventsService.fireEvent(AppEvents.ON_OFFLINE);
      });

      this.appSyncClient.onError().subscribe(error=>{
        this.toastController.create({
                                message:error,
                                duration : 10000
                            })
                            .then(toast=>{
                              toast.present();
                            });
      });
    });
  }


  private isOnLine(){
    let result = false;
    if(this.network.type!=null){
      let type = this.network.type;
      result =  type != this.network.Connection.UNKNOWN &&  
                type != this.network.Connection.NONE;
    }
    return result;
  }
}
