import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import {Tab1Page} from './tab1/tab1.page';
import {RentDetailView} from './tab1/views/rent_detail_view/rent.detail.view';

import {Tab2Page} from './tab2/tab2.page';
import {DisasterCreateView} from './tab2/views/disaster_create_view/disaster.create.view';
import {DisasterDetailView} from './tab2/views/disaster_detail_view/disaster.detail.view';

import {Tab3Page} from './tab3/tab3.page';
const routes: Routes = [
  { path: '',component: Tab1Page },
  { path: 'tab1', component: Tab1Page },
  { path: 'tab1/:uid', component: RentDetailView },
  { path: 'tab2', component: Tab2Page },
  { path: 'tab2/create', component: DisasterCreateView },
  { path: 'tab2/view/:uid', component: DisasterDetailView },
  
  { path: 'tab3', component: Tab3Page },
  
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
