
export function normalize(value:string):string {
        let TAB_00C0 = "AAAAAAACEEEEIIIIDNOOOOO*OUUUUYIs" +
        "aaaaaaaceeeeiiii?nooooo/ouuuuy?y" +
        "AaAaAaCcCcCcCcDdDdEeEeEeEeEeGgGg" +
        "GgGgHhHhIiIiIiIiIiJjJjKkkLlLlLlL" +
        "lLlNnNnNnnNnOoOoOoOoRrRrRrSsSsSs" +
        "SsTtTtTtUuUuUuUuUuUuWwYyYZzZzZzF";
        var result = value.split('');
        for (var i = 0; i < result.length; i++) {
            var c = value.charCodeAt(i);
            if (c >= 0x00c0 && c <= 0x017f) {
                result[i] = String.fromCharCode(TAB_00C0.charCodeAt(c - 0x00c0));
            } else if (c > 127) {
                result[i] = '_';
            }
        }

        return result.join('').replace(/\W/g, '_');
    }