//==============================================================================
// RENT QUERIES
//==============================================================================
export const LIST_ARENT_QUERY = `query ListARents($filter: ModelARentFilterInput, $limit: Int, $nextToken: String) {
    listARents(filter: $filter, limit: $limit, nextToken: $nextToken) {
      __typename
      items {
        __typename
        id
        name
        type
        price
        description
        thumbnail {
          __typename
          id
          source
        }
        disasters {
          __typename
          nextToken
        }
      }
      nextToken
    }
  }`;


export const GET_ARENT_QUERY =`query GetARent($id: ID!) {
  getARent(id: $id) {
    __typename
    id
    name
    type
    price
    description
    thumbnail {
      __typename
      id
      source
      rent {
        __typename
        id
        name
        type
        price
        description
      }
      comment {
        __typename
        id
        login
        content
        status
      }
    }
    disasters {
      __typename
      items {
        id
        date
        type {
          id
          value
        }
      }
      nextToken
    }
  }
}`;
//==============================================================================
// DISASTER QUERIES
//==============================================================================
export const LIST_ADISASTER_QUERY =`query ListADisasters($filter: ModelADisasterFilterInput, $limit: Int, $nextToken: String) {
    listADisasters(filter: $filter, limit: $limit, nextToken: $nextToken) {
      __typename
      items {
        __typename
        id
        rent {
          __typename
          id
          name
          type
          price
          description
        }
        date
        description
        type {
          __typename
          id
          value
        }
        status {
          __typename
          id
          value
        }
        comments {
          __typename
          nextToken
        }
      }
      nextToken
    }
  }
`

export const CREATE_ADISASTER_INPUT =`input CreateADisasterInput{
    id                  : ID
    date                : AWSTimestamp
    aDisasterRentId     : String
    aDisasterTypeId     : String
    aDisasterStatusId   : String
    description         : String
}
`
export const CREATE_ADISASTER_QUERY = `mutation CreateADisaster($input: CreateADisasterInput!) {
    createADisaster(input: $input) {
      __typename
      id
      date
      description
      rent {
        id
        name
        type
        price
        description
        thumbnail {
          id
          source
        }
        disasters {
          nextToken
        }
      }
      type {
        id
        value
      }
      status {
        id
        value
        disaster {
          id
          date
        }
      }
    }
  }`

export const DELETE_ADISASTER_QUERY =`mutation DeleteADisaster($input: DeleteADisasterInput!) {
  deleteADisaster(input: $input) {
    __typename
    id
    rent {
      __typename
      id
      name
      type
      price
      description
      thumbnail {
        __typename
        id
        source
      }
      disasters {
        __typename
        nextToken
      }
    }
    date
    type {
      __typename
      id
      value
    }
    status {
      __typename
      id
      value
      disaster {
        __typename
        id
        date
      }
    }
    comments {
      __typename
      items {
        __typename
        id
        login
        content
        status
      }
      nextToken
    }
  }
}`;


export const DELETE_ADISASTER_INPUT =`input CreateADisasterInput{
  id  : ID!
}
`

export const GET_ADISASTER_QUERY =`query GetADisaster($id: ID!) {
  getADisaster(id: $id) {
    __typename
    id
    description
    rent {
      __typename
      id
      name
      type
      price
      description
      thumbnail {
        __typename
        id
        source
      }
      disasters {
        __typename
        nextToken
      }
    }
    date
    type {
      __typename
      id
      value
    }
    status {
      __typename
      id
      value
      disaster {
        __typename
        id
        date
      }
    }
    comments {
      __typename
      items {
        __typename
        id
        login
        content
        status
      }
      nextToken
    }
  }
}`


export const UPDATE_ADISASTER_QUERY = `mutation UpdateADisaster($input: UpdateADisasterInput!) {
  updateADisaster(input: $input) {
    __typename
    id
    rent {
      __typename
      id
      name
      type
      price
      description
      thumbnail {
        __typename
        id
        source
      }
      disasters {
        __typename
        nextToken
      }
    }
    date
    type {
      __typename
      id
      value
    }
    status {
      __typename
      id
      value
      disaster {
        __typename
        id
        date
      }
    }
    comments {
      __typename
      items {
        __typename
        id
        login
        content
        status
      }
      nextToken
    }
  }
}
`;
//==============================================================================
// DISASTER TYPE QUERIES
//==============================================================================
export const CREATE_ADISASTER_TYPE_QUERY = `mutation CreateADisasterType($input: CreateADisasterTypeInput!) {
    createADisasterType(input: $input) {
      __typename
      id
      value
    }
  }`;


export const CREATE_ADISASTER_TYPE_INPUT =`input CreateADisasterTypeInput{
    id                          : ID
    value                       : String
    aDisasterTypeDisasterId     : String
}`;

export const LIST_ADISASTER_TYPE =`query ListADisasterTypes($filter: ModelADisasterTypeFilterInput, $limit: Int, $nextToken: String) {
    listADisasterTypes(filter: $filter, limit: $limit, nextToken: $nextToken) {
      __typename
      items {
        __typename
        id
        value
      }
      nextToken
    }
  }
`;

export const LIST_ADISASTER_TYPE_INPUT =`input ModelADisasterTypeFilterInput{
    id                          : ID
    value                       : String
    aDisasterTypeDisasterId     : String
}`;