import {Injectable}         from '@angular/core';
import {isNotNull,isNull}   from './../../tools/checks';
import {Rent,
        RentType,
        Disaster,
        DisasterType,
        DisasterStatus,
        Comment}            from './../../models/commons.models.interfaces';

export class AppSyncEntitiesMapperService{

    //==========================================================================
    // MAPPERS
    //==========================================================================
    public mapRentFromEntity(entity:any):Rent{
        let result = null;
        if(entity !=null){
            result= {
                id              : entity.id,
                name            : entity.name,
                price           : entity.price,
                type            : this.resolveRentType(entity.type),
                description     : entity.description,
                thumbnail       : null,
                disasters       : null
            };

            if(isNotNull(entity.thumbnail)&& isNotNull(entity.thumbnail.source)){
                result.thumbnail = {
                    id      : entity.thumbnail.id,
                    source  : entity.thumbnail.source
                };
            }

            if(isNotNull(entity.disasters) && isNotNull(entity.disasters.items)){
                let disasters = [];
                for(let disaster of entity.disasters.items){
                    disasters.push(this.mapDisasterFromEntity(disaster));
                }

                result.disasters = disasters.filter(item=>isNotNull(item));
            }
        }
        return result;
    }

    public mapDisasterFromEntity(entity:any):Disaster{
        let result = null;
        if(isNotNull(entity)){
            result ={
                id:entity.id,
                rendUid:isNull(entity.rent)?null:entity.rent.id,
                date:entity.date,
                type:{
                    id:this.resolveDisasterType(entity.type),
                    uid:entity.id
                },
                status:this.resolveDisasterStatus(entity.status),
                comments:this.mapCommentsFromEntity(entity.comments),
                description:entity.description
            };
        }
        return result;
    }

    public mapCommentsFromEntity(entity:any):Comment[]{
        return null;
    }
    //==========================================================================
    // RESOLVERS
    //==========================================================================
    private resolveRentType(value:any):RentType{
        let result = null;

        if(isNotNull(value)){
            result = RentType[value];
        }
        return isNull(result)?RentType.UNDEFINE:result;
    }

    private resolveDisasterType(entity:any):DisasterType{
        let result = null;
        if(isNotNull(entity) && isNotNull(entity.value)){
            result = entity.value;
        }
        return result;
    }

    private resolveDisasterStatus(value:any):DisasterStatus{
        let result = null;
        if(isNotNull(value)){
            result =  RentType[value];
        }
        return  isNull(result)?DisasterStatus.undefine:result;
    }

    
}