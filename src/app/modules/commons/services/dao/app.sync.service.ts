import {Injectable} from '@angular/core';
import {AWSAppSyncClient, AUTH_TYPE} from 'aws-appsync';
import awsmobile from './../../../../../aws-exports';
import { Auth } from 'aws-amplify';
import { isNotNull } from '../../tools/checks';
import {Observable,Subscriber} from 'rxjs';

@Injectable()
export class AppSyncService{

    private appSync : AWSAppSyncClient<any>;
    private onErrorSubscriber : Subscriber<string>;
    private onErrorHandler : Observable<string> = new Observable(observer=>this.onErrorSubscriber=observer);
    
    constructor(){
        const builder = new AWSAppSyncClient({
            url:awsmobile.aws_appsync_graphqlEndpoint,
            region:awsmobile.aws_appsync_region,
            auth : {
                type:AUTH_TYPE.AMAZON_COGNITO_USER_POOLS,
                jwtToken: async ()=> (await Auth.currentSession()).getIdToken().getJwtToken()
            },
            offlineConfig:{
                callback: (err: any, success: any)=>this.handlerOfflineCallback(err,success)
            }
        });
        this.appSync = builder;
    }


    client() :  Promise<AWSAppSyncClient<any>>{
        return this.appSync.hydrated();
    }

    private handlerOfflineCallback(error:any, success:any){
        if(isNotNull(error)){
            this.onErrorSubscriber.next("error on invoke mutation : "+error.mutation);
        }
    }

    public onError():Observable<string>{
        return this.onErrorHandler;
    }
}
