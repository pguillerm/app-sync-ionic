import {Injectable}                 from '@angular/core';
import {Rent, Disaster}             from './../../models/commons.models.interfaces';
import {AppSyncDao}                 from './app.sync.dao';
import { isNotNull } from '../../tools/checks';


export interface Dao{
    findAllRents()                          : Promise<Rent[]>;
    findAllDisasters()                      : Promise<Disaster[]>;
    saveDisaster(data:Disaster)             : Promise<any>;
    getDisaster(uid:string)                 : Promise<Disaster>;
    deleteDisaster(uid:string)              : Promise<any>;
    updateDisaster(data:Disaster)           : Promise<any>;
    findRent(uid:string)                    : Promise<Rent>;
}


@Injectable()
export class DaoFactory {
    
    /**************************************************************************
    * ATTRIBUTES
    **************************************************************************/
    private daoProxy : Dao = null;
    private handler : any ;


    /**************************************************************************
    * CONSTRUCTORS
    **************************************************************************/
    constructor(private daoAwsAppSync:AppSyncDao) {
        /*
        let self=this;
        this.handler = {
            get: (object,method)=>{
               return this.intercept("get",object, method);
            },
            set:(object,method, args)=>{
                return this.intercept("set",object, method,args);
            },
            apply:(object,method, args)=>{
                return this.intercept("apply",object, method,args);
            }
        };

        this.daoProxy = new Proxy(this.daoAwsAppSync, this.handler);
        */
       this.daoProxy = this.daoAwsAppSync;
    }

    /**************************************************************************
    * INTERCEPTOR
    **************************************************************************/
    intercept(flag:string,object:any, method:string, values?:any){
        console.log("intercept :",[flag,object,method,values]);
        

        let invoke = method in object? object[method] : null;
        let result = invoke; 

        if(object instanceof  AppSyncDao && isNotNull(result) && this.isNotExclude(method) && typeof invoke === "function" ){
            result = new Proxy(invoke, this.handler);
        }
        /*
        if(invoke!=undefined && invoke !=null &&  typeof object === "Dao" &&  typeof invoke === "function" ){
            result = new Proxy(invoke, this.handler);
        }
        */
        return result;
    }

    private isNotExclude(method:string){
        return method!=="toString" && method!=="Symbol(Symbol.toPrimitive)";
    }
    /**************************************************************************
    * GET DAO
    **************************************************************************/
    public getDao():Dao{
        return this.daoProxy;
    }


}