import {Injectable}                                 from '@angular/core';
import {environment}                                from './../../../../../environments/environment';
import {isNotNull, isNull}                          from '../../tools/checks';
import {EventsServices,AppEvents}                   from './../../services/events.services';
import {Dao}                                        from './dao.factory';
import {AppSyncEntitiesMapperService}               from './app.sync.entities.mapper.service';
import {AppSyncService}                             from './app.sync.service';
import {AWSAppSyncClient, buildMutation}            from 'aws-appsync';
import {CacheService}                               from './../cache/cache.service';

import gql                                          from 'graphql-tag';

import {RENTS,
        DISASTERS,
        getDisastersCounter}                        from './mock.data';


import {APIService,
        CreateADisasterInput,
        ListADisasterTypesQuery}                    from './API.service';        

import {Rent,
        Disaster,
        DisasterStatus,
        DisasterType
        }                                           from './../../models/commons.models.interfaces';

import {CREATE_ADISASTER_QUERY,
        CREATE_ADISASTER_INPUT,
        LIST_ADISASTER_QUERY,
        LIST_ARENT_QUERY,
        CREATE_ADISASTER_TYPE_QUERY,
        CREATE_ADISASTER_TYPE_INPUT,
        LIST_ADISASTER_TYPE,
        DELETE_ADISASTER_INPUT,
        DELETE_ADISASTER_QUERY,
        GET_ARENT_QUERY,
        GET_ADISASTER_QUERY,
        UPDATE_ADISASTER_QUERY
        }                                           from './app.sync.queries';


@Injectable()
export class AppSyncDao implements Dao{

    

    /**************************************************************************
    * CONSTRUCTORS
    **************************************************************************/
    constructor(private eventsService   : EventsServices,
                private apiService      : APIService,
                private appSync         : AppSyncService,
                private mapper          : AppSyncEntitiesMapperService,
                private cacheService    : CacheService) {

        

        this.eventsService.on(AppEvents.PAUSE).subscribe((event)=> console.log(">>>>> Pause >>>>>"));
        this.eventsService.on(AppEvents.RESUME).subscribe((event)=> console.log(">>>>> RESUME >>>>>"));
        
        this.eventsService.on(AppEvents.ON_OFFLINE).subscribe((event)=> console.log(">>>>> DISCONNECTED >>>>>"));
        this.eventsService.on(AppEvents.ON_ONLINE).subscribe((event)=> console.log(">>>>> CONNECTED >>>>> ",JSON.stringify(event)));

       
    }

    /**************************************************************************
    * FIND
    **************************************************************************/
    findAllRents() :Promise<Rent[ ]>{
       return new Promise((resolve,reject)=>{
            this.appSync.client().then(client=>{
                client.watchQuery({
                    query:gql(LIST_ARENT_QUERY),
                    fetchPolicy: 'cache-and-network'
                }).subscribe(
                    (response)=>{
                        if(isNotNull(response.data)){
                            let items= response.data['listARents'].items;
                            let result = items.map(   item =>this.mapper.mapRentFromEntity(item))
                                              .filter(item => item!=null);
                            resolve(result);
                        }else{
                            resolve(null);
                        }
                        
                    },
                    (error)=>this.handlerError(error,reject,"findAllRents")
                )
            });
        });
    }

    findAllDisasters() :  Promise<Disaster[]>{
        return new Promise((resolve,reject)=>{
            
            this.appSync.client().then(client=>{

                client.watchQuery({
                    query:gql(LIST_ADISASTER_QUERY),
                    fetchPolicy: 'cache-and-network'
                }).subscribe(
                    (response)=>{
                        let data = response.data;
                        let items= isNull(data)?[]:response.data['listADisasters'].items;
                        let result = items.map(item=>this.mapper.mapDisasterFromEntity(item))
                                          .filter(item => item!=null);
                        resolve(result);
                    },
                    (error)=>{
                        console.log("error : ",error);
                        this.handlerError(error,reject,"findAllDisasters");
                    }
                )
            });
        }); 
    }


    getDisaster(uid: string): Promise<Disaster> {
        return new Promise((resolve,reject)=>{
            this.appSync.client().then(client=>{
                client.watchQuery({
                    query:gql(GET_ADISASTER_QUERY),
                    fetchPolicy: 'cache-and-network',
                    variables:{
                        id:uid
                    }
                }).subscribe(
                    (response)=>{
                        let result = null;
                        if(isNotNull(response.data)){
                            result = this.mapper.mapDisasterFromEntity(response.data['getADisaster']);
                        }
                        
                        resolve(result);
                    },
                    (error)=>this.handlerError(error,reject,"getDisaster")
                )
            });
        }); 
    }
    
    private searchDisasterType(disasterType:string, appSyncClient:AWSAppSyncClient<any>) : Promise<string>{
        return new Promise((resolve,reject)=>{
            /*
            appSyncClient.watchQuery({
                query:gql(LIST_ADISASTER_TYPE),
                fetchPolicy: 'cache-and-network',
                variables:{
                    filter:{
                        value:{eq:disasterType}   
                    }
                }
                
            }).subscribe(
                (response)=>{
                    if(isNull(response.data) || isNull(response.data['listADisasterTypes']) ||response.data['listADisasterTypes'].items.length==0){
                        resolve(null);
                    }else{
                        let items = response.data['listADisasterTypes'].items;
                        resolve(items[0].id)
                    }
                   
                },
                (error)=>{
                    this.handlerError(error,reject,"searchDisasterType");
                }
            )
            */
            this.cacheService.load("disasterType")
                             .then(data=>{
                                if(this.eventsService.isOnline){
                                   this.apiService.ListADisasterTypes()
                                                  .then(values=>{
                                                      
                                                      if(isNotNull(values.items)){
                                                        this.cacheService.save("disasterType",values.items);  
                                                      }
                                                      let result = this.searchDiasterTypeInList(disasterType,values.items);
                                                      resolve(result);
                                                  })
                                }else{
                                    resolve(this.searchDiasterTypeInList(disasterType,data));
                                }
                             });
        });
    }

    private searchDiasterTypeInList(name:string,values:any[]):string{
        let result = null;
        if(isNotNull(values)){
            let search = values.filter(item=>item.value==name);
            result= search.length>0?search[0].id:null;
        }
        return result;
    }
        

 findRent(uid: string): Promise<Rent> {
        return new Promise((resolve,reject)=>{
            this.appSync.client().then(client=>{
                 client.watchQuery({
                    query:gql(GET_ARENT_QUERY),
                    fetchPolicy: 'cache-and-network',
                    variables:{
                        id:uid
                    }
                }).subscribe(
                    (response)=>{
                        let result = isNull(response.data)?null:this.mapper.mapRentFromEntity(response.data['getARent']);
                        resolve(result);
                    },
                    (error)=>this.handlerError(error,reject,"findRent")
                )
            });
        });
    }
  

    findAllDisastersByRend(uid: string): Promise<Disaster[]> {
        return new Promise((resolve,reject)=>{
            this.apiService.GetARent(uid)
                .then(rentEntity=>{
                    let result = null;
                    if(isNotNull(rentEntity.disasters)){
                        result = rentEntity.disasters
                                           .items
                                           .map(entity => this.mapper.mapDisasterFromEntity(entity))
                                           .filter(item=>isNotNull(item))
                    }

                    resolve(result);
                })
                .catch(error=>this.handlerError(error,reject));
        });
    }

    //==========================================================================
    // SAVE DISASTER
    //==========================================================================
    saveDisaster(data: Disaster): Promise<Disaster> {  
        return new Promise((resolve,reject)=>{
            this.appSync.client()
                        .then(client=>{
                            this.saveDisasterType(data,client)
                                .then(typeId=>{
                                    console.log("typeId : ",typeId);
                                    this.processSaveDisaster(data,client,typeId)
                                        .then(response => {
                                            resolve(response);
                                        })
                                        .catch(error=>this.handlerError(error,reject,"saveDisaster.processSaveDisaster"));
                                })
                                .catch(error=>this.handlerError(error,reject,"saveDisaster.saveDisasterType"));
                          
                        })
                        .catch(error=>this.handlerError(error,reject,"saveDisaster.client"));
        });
    }

    private processSaveDisaster(data: Disaster,appSyncClient:AWSAppSyncClient<any>, disasterTypeId:string): Promise<any>{

        return new Promise((resolve,reject)=>{
            let query = buildMutation(
                appSyncClient,
                gql(CREATE_ADISASTER_QUERY),
                {
                    input:{
                        aDisasterStatusId: DisasterStatus[data.status],
                        aDisasterRentId:data.rendUid,
                        date:data.date,
                        aDisasterTypeId:disasterTypeId
                    }
                },
                _cache => [gql(LIST_ARENT_QUERY),gql(LIST_ADISASTER_QUERY)],
                "ADisaster"
            );
            
            appSyncClient.mutate(query)
                         .then(response=>{
                             resolve(response);
                          })
                          .catch(error=>this.handlerError(error,reject,"saveDisasterType.processSaveDisaster"));
        });
    }


    private saveDisasterToAws(data: Disaster, disasterTypeId:string): Promise<any>{
        return new Promise((resolve,reject)=>{
            this.appSync.client().then(appSyncClient=>{
                let query = buildMutation(
                    appSyncClient,
                    gql(CREATE_ADISASTER_QUERY),
                    {
                        input:{
                            aDisasterStatusId: DisasterStatus[data.status],
                            aDisasterRentId:data.rendUid,
                            date:data.date,
                            aDisasterTypeId:disasterTypeId
                        }
                    },
                    _cache => [gql(LIST_ARENT_QUERY),gql(LIST_ADISASTER_QUERY)],
                    "ADisaster"
                );
                
                appSyncClient.mutate(query)
                             .then(response=>{
                                 resolve(response);
                              })
                              .catch(error=>this.handlerError(error,reject,"saveDisasterType.processSaveDisaster"));
            });
        });
    }

    private saveDisasterType(data: Disaster,appSyncClient:AWSAppSyncClient<any>): Promise<string>{
        return new Promise((resolve,reject)=>{
            if(isNull(data.type)){
                resolve(null);
            }else{
                this.searchDisasterType(data.type.id,appSyncClient)
                    .then(disasterTypeId=>{
                        if(isNull(disasterTypeId)){
                            this.processSaveDisasterType(data.type.id,appSyncClient)
                                .then(newDisasterTypeId=>resolve(newDisasterTypeId))
                                .catch(error=>this.handlerError(error,reject,"saveDisasterType.processSaveDisasterType"));
                        }else{
                            resolve(disasterTypeId);
                        }
                    })
                    .catch(error=>this.handlerError(error,reject,"saveDisasterType.searchDisasterType"));
            }
        });
    }





    private processSaveDisasterType(disasterType:string, appSyncClient:AWSAppSyncClient<any>) : Promise<string>{
        return new Promise((resolve,reject)=>{
            let query = buildMutation(
                appSyncClient,
                gql(CREATE_ADISASTER_TYPE_QUERY),
                {
                    input:{
                        value:disasterType
                    }
                },
                _cache => gql(LIST_ADISASTER_TYPE),
                "ADisasterType"
            );
            
            appSyncClient.mutate(query)
                         .then(response=>{
                             console.log(">> save DisasterType : ",response);
                             resolve(response.data.createADisasterType.id);
                            })
                         .catch(error=>{
                             this.handlerError(error,reject,"processSaveDisasterType");
                        });
        });
    }
    
    
    //==========================================================================
    // DELETE DISASTER
    //==========================================================================
    deleteDisaster(uid: string): Promise<any> {
        return new Promise((resolve,reject)=>{
            this.appSync.client()
                        .then(client=>{
                            this.processDeleteDisaster(uid,client)
                                .then(data=>resolve())
                                .catch(error=>this.handlerError(error,reject));
                        })
                        .catch(error=>this.handlerError(error,reject,"deleteDisaster"));
        });
    }

    private processDeleteDisaster(uid:string,appSyncClient:AWSAppSyncClient<any>): Promise<any> {
        return new Promise((resolve,reject)=>{
            let query = buildMutation(
                        appSyncClient,
                        gql(DELETE_ADISASTER_QUERY),
                        {
                            input:{
                                id:uid
                            }
                        },
                        _cache => gql(LIST_ADISASTER_QUERY),
                        "ADisaster"
            );

            
            appSyncClient.mutate(query)
                         .then(response=>resolve())
                         .catch(error=>this.handlerError(error,reject,"processDeleteDisaster"));
        });  
    }

    //==========================================================================
    // UPDATE DISASTER
    //==========================================================================
    updateDisaster(data:Disaster)           : Promise<any>{
        return new Promise((resolve,reject)=>{
            this.appSync.client()
            .then(client=>{
                this.processUpdateDisaster(data,client)
                    .then(()=>resolve())
                    .catch(error=>this.handlerError(error,reject,"processUpdateDisaster"));
            })
            .catch(error=>this.handlerError(error,reject,"updateDisaster"));
        });
    }
    private processUpdateDisaster(data:Disaster,appSyncClient:AWSAppSyncClient<any>): Promise<any> {
        return new Promise((resolve,reject)=>{
            
            let query = buildMutation(
                appSyncClient,
                gql(UPDATE_ADISASTER_QUERY),
                {
                    input:{
                        id:data.id,
                        description:data.description,
                        aDisasterStatusId: DisasterStatus[data.status],
                        aDisasterRentId:data.rendUid,
                        date:data.date
                    }
                },
                _cache => [gql(LIST_ADISASTER_QUERY), gql(GET_ADISASTER_QUERY)],
                "ADisaster"
                );

            appSyncClient.mutate(query)
                         .then(response=>resolve())
                         .catch(error=>this.handlerError(error,reject,"processDeleteDisaster"));
        });  
    }

    //==========================================================================
    // EXCEPTIONS
    //==========================================================================
    private handlerError(error:any, reject:any, flag?:string){
        let msg = "ERROR ";
        if(isNotNull(flag)){
            msg = [msg,'[',flag,'] '].join('');
        }
        console.error(msg,error);
        if(isNotNull(reject)){
            reject(error);
        }
    }
}
