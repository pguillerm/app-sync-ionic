import {Injectable}                 from '@angular/core';
import {Events}                     from '@ionic/angular';
import { Observable } from 'rxjs';

export enum AppEvents {
    PAUSE,
    RESUME,
    ON_OFFLINE,
    ON_ONLINE,
    SUB_APPLICATION_START,
    SUB_APPLICATION_DONE
}

export interface ApplicationEvent{
    data:any,
    time:number,
    subApplication:boolean
}

@Injectable()
export class EventsServices {
    /**************************************************************************
    * CONSTRUCTOR
    **************************************************************************/
    public isOnline         : boolean;
    public subAppRunning    : boolean = false;
    /**************************************************************************
     * CONSTRUCTOR
    **************************************************************************/
    constructor(private events:Events){}

    /**************************************************************************
     * SERVICE
     **************************************************************************/
    public fireEvent(event:AppEvents, data?:any){
        switch(event){
            case AppEvents.SUB_APPLICATION_START :
                this.subAppRunning = true;
                break;
            case AppEvents.SUB_APPLICATION_DONE:
                this.subAppRunning = false;
                break;

            case AppEvents.ON_ONLINE:
                this.isOnline = true;
                break;

            case AppEvents.ON_OFFLINE:
                this.isOnline = false;
                break;
        }

        this.events.publish(AppEvents[event],data,Date.now());
    }
    
    public on(event:AppEvents):Observable<ApplicationEvent>{
        return new Observable((observer)=>{
            this.events.subscribe(AppEvents[event],(values,timestamp)=>{
                observer.next({
                    data:values,
                    time:timestamp,
                    subApplication:this.subAppRunning
                });
            });
        });
    }
}