import {Injectable}             from '@angular/core';

import {isNotNull,isNull}       from './../../tools/checks';


@Injectable()
export class CacheService{

    /**************************************************************************
    * ATTRIBUTES
    **************************************************************************/
    private buffer : Map<string,any> = new Map();

    /**************************************************************************
    * CONSTRUCTORS
    **************************************************************************/
    constructor() {
    }

    /**************************************************************************
    * API
    **************************************************************************/
    load(cacheKey : string):Promise<any>{
        return new Promise((resolve,reject)=>{
            let result = null;
            if(isNotNull(cacheKey)){
                result = this.buffer.get(cacheKey);
                if(isNull(result)){
                    let rawData = localStorage.getItem(cacheKey);
                    try{
                        result =  isNull(rawData)?null : JSON.parse(rawData);
                        this.buffer.set(cacheKey,result);
                    }catch(error){
                        reject(error);
                    }  
                }
            }
            resolve(result);
        });
    }

    save(cacheKey : string, value : any):Promise<any>{
        return new Promise((resolve,reject)=>{
            if(isNull(cacheKey)){
                reject("cache key is mandatory!");
            }
            
            if(isNotNull(value)){
                this.buffer.set(cacheKey,value);
                localStorage.setItem(cacheKey,JSON.stringify(value));
                resolve();
            }
        });
    }

    remove(cacheKey : string):Promise<any>{
        return new Promise((resolve,reject)=>{
            if(isNull(cacheKey)){
                reject("cache key is mandatory!");
            }

            this.buffer.delete(cacheKey);
            localStorage.removeItem(cacheKey);
            resolve();
        });
    }
}