import {NgModule}                   from '@angular/core';
import {FontAwesomeModule}          from '@fortawesome/angular-fontawesome';
import {AppSyncDao}                 from './services/dao/app.sync.dao';
import {DaoFactory}                 from './services/dao/dao.factory';
import {EventsServices}             from './services/events.services';
import {NetworkStatus}              from './components/network_status/network.status';

@NgModule({
    declarations: [NetworkStatus],
    exports:[NetworkStatus],
    entryComponents: [],
    imports: [FontAwesomeModule],
    bootstrap: []
  })
  export class CommonsModule {}
  