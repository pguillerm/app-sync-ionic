
import {Component, OnInit,NgZone}        from '@angular/core';

import {EventsServices,AppEvents}   from './../../services/events.services';
@Component({
    selector      : 'network-status',
    template      : `<span [class]="networkStatus"></span>`,
    styleUrls     : ['./network.status.scss']
  })
  export class NetworkStatus implements OnInit{
   

   
    networkStatus : string = "network-status unknown";


    constructor(private eventsService:EventsServices,
                private zone:NgZone ){
    }

    ngOnInit(): void {
        this.eventsService.isOnline?this.setOnline():this.setOffline();

        this.eventsService
            .on(AppEvents.ON_OFFLINE)
            .subscribe((event)=>this.setOffline());

        this.eventsService
            .on(AppEvents.ON_ONLINE)
            .subscribe((event)=>this.setOnline());
    }
  
    
    setOnline(){
        this.changeStatus("network-status online");
    }
    setOffline(){
        this.changeStatus("network-status offline");
    }
    changeStatus(status:string){
        console.log("change status :"+status);
        
        this.zone.run(()=>{
            this.networkStatus=status;
        });
    }
  }