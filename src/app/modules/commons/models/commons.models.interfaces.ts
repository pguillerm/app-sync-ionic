import { unescapeIdentifier } from '@angular/compiler';

export interface Rent{
    id           : string,
    name         : string,
    type         : RentType,
    price        : number,
    description? : string
    thumbnail?   : Image,
    disasters?   : Disaster[]
}

export enum RentType{
    FLAT,
    HOME,
    OFFICE,
    PARKING,
    UNDEFINE
}

export interface Image {
    id     : string,
    source : string
}

export interface User {
    login       : string,
    role        : string,
    firstName   : string,
    lastName    : string,
    phoneNumber : string,
    gavatar     : Image
}

export interface Customer extends User{
    rents : Rent[]
}

export interface Support extends User{
    functionName : string
}

export interface Disaster {
    id?         : string,
    rendUid     : string,
    date        : number,
    type        : DisasterType,
    status      : DisasterStatus,
    comments?   : Comment[],
    description?: string
}
export enum DisasterStatus{
    unread,
    read,
    validate,
    reject,
    close,
    undefine
}

export interface DisasterType{
    id     : string
    uid?   : string
}

export interface Comment{
    id          : string,
    login       : string,
    content?    : string,
    images?     : Image[],
    status?     : CommentStatus
}

export enum CommentStatus{
    unread,
    read
}