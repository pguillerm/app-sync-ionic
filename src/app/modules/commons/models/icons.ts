import {faBuilding,
        faHouseDamage,
        faHome,
        faParking,
        faLandmark,
        faCheckCircle,
        faLightbulb,
        faWater,
        faThermometerQuarter,
        faEnvelope,
        faEnvelopeOpen,
        faTimesCircle,
        faDoorClosed,
        faTrashAlt,
        faCaretSquareLeft
        
    }   from '@fortawesome/free-solid-svg-icons';
import {IconDefinition} from '@fortawesome/fontawesome-common-types';



export const Icons = {
    office                  : faLandmark,
    home                    : faHome,
    homeDomage              : faHouseDamage,
    flat                    : faBuilding,
    parking                 : faParking,
    defaultDisasterStatus   : faCheckCircle,
    defaultDisasterIcon     : faHouseDamage,
    plumbing                : faWater,
    electricity             : faLightbulb,
    heating                 : faThermometerQuarter,

    unread                  : faEnvelope,
    read                    : faEnvelopeOpen,
    validate                : faCheckCircle,
    reject                  : faTimesCircle,
    close                   : faDoorClosed,
    delete                  : faTrashAlt,
    goBack                  : faCaretSquareLeft
}

export function getIcon(name : string):IconDefinition{
    let result = null;
    if(name!=undefined && name !=null){
        result = Icons[name];
    } 
    return result;
}